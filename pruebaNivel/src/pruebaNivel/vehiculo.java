package pruebaNivel;

public class vehiculo {
	String marca;
	String matricula;
	char tipo; /* "C" Moche, "M" Moto, "O" Otro*/
	double precio;
	double preciovip;
	
	public vehiculo(String marca, String matricula, char tipo, double precio, double preciovip) {
		super();
		this.marca = marca;
		this.matricula = matricula;
		this.tipo = tipo;
		this.precio = precio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getPreciovip() {
		return preciovip;
	}

	public void setPreciovip(double preciovip) {
		this.preciovip = preciovip;
	}

	@Override
	public String toString() {
		return "vehiculo [marca=" + marca + ", matricula=" + matricula + ", tipo=" + tipo + ", precio=" + precio
				+ ", preciovip=" + preciovip + "]";
	}
	
}
