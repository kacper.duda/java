package pruebaNivel;

public class coche extends vehiculo {
	int npuertas;
	String modelo;
	
	public coche(String marca, String matricula, char tipo, double precio, double preciovip, int npuertas,
			String modelo) {
		super(marca, matricula, tipo, precio, preciovip);
		this.npuertas = npuertas;
		this.modelo = modelo;
	}
	
	public int getNpuertas() {
		return npuertas;
	}

	public void setNpuertas(int npuertas) {
		this.npuertas = npuertas;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "coche [npuertas=" + npuertas + ", modelo=" + modelo + "]";
	}
	
}
