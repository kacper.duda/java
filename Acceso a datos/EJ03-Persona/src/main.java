import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class main {
	
	static String ficheroPersonas = "personas.txt";
	
	static Persona[] persona = {
			new Persona("Pepe", "Gonzalez", 20), 
			new Persona("Jose", "Fernandez", 26), 
			new Persona("Antonio", "Tomas", 15),
			new Persona("Luis", "Perez", 10),
			new Persona("Juan", "Antonio", 30)
	};

	public static void EscribirFichero() {
		try {
			File fichero = new File(ficheroPersonas);
			FileWriter fw = new FileWriter(fichero);
			
			for (int i=0; i< persona.length; i++) {
				fw.write(persona[i].getNombre() + "," + persona[i].getApellido() + "," + persona[i].getEdad() + ";\n");
			}
			
			fw.close();
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void LeerFichero() {
		try {
			
			File fichero = new File(ficheroPersonas);
			FileReader fr = new FileReader(fichero);
			
			int letra;
			int pos = 0;
			String cadena = "";
			
			letra = fr.read();
			
			while (letra != -1) {
				if (letra == '\n') {
					pos++;
				} else {
					cadena += ((char) letra);
				}
				letra = fr.read();
			}
			fr.close();
			System.out.println("He le�do: " + cadena);
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());	
		}
	}
	
	public static void main(String[] args) {
		int opcion = 0;
		do {
			System.out.println("OPCIONES:");
			System.out.println("=========================================");
			System.out.println("Opcion 1: Escribir fichero");
			System.out.println("Opcion 2: Leer fichero");
			System.out.println("Opcion 3: Mostrar por pantalla");
			System.out.println("Opcion 4: Vaciar estructura");
			System.out.println("Opcion 5: Salir");
			System.out.println("=========================================");
			System.out.print("Selecciona una opci�n: ");
			
			Scanner numOpcion = new Scanner(System.in);
			opcion = numOpcion.nextInt();
			
			switch (opcion) {
				case 1:
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 1: Escribir fichero\n");
					EscribirFichero();
					System.out.println("Se ha insertado en el fichero de texto correctamente.\n");
					for (int i=0; i< persona.length; i++) {
						System.out.println(persona[i].getNombre() + "," + persona[i].getApellido() + "," + persona[i].getEdad() + ";");
					}
					System.out.println("-----------------------------------------");
					break;
				case 2:
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 2: Leer fichero\n");
					LeerFichero();
					System.out.println("-----------------------------------------");
					break;
				case 3:
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 3: Mostrar por pantalla\n");
					System.out.println("Estructura: \n");
					for (int i=0; i< persona.length; i++) {
						System.out.println(persona[i] + "");
					}
					System.out.println("-----------------------------------------");
					break;
				case 4:
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 4: Vaciar estructura\n");
					for (int i=0; i< persona.length; i++) {
						persona[i] = new Persona("","",0);
					}
					System.out.println("Se ha vaciado correctamente la estructura.\n");
					System.out.println("-----------------------------------------");
					break;
				case 5:
					break;
				default:
					System.out.println("\n�No existe esta opci�n escoge otra!\n");
					break;
			}
		} while (opcion !=5);
		
		
	}

}
