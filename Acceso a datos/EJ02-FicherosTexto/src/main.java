import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class main {

	static String ficheroNumeros = "numeros.txt"; 
	static int[] tablaNumeros = {11,22,33,444,555,6666,77,8,99,000};
	
	public static void Inicializar(int[] tabla) {
		
		for (int i=0; i<tabla.length; i++) {
			tabla[i] = 0;
		}
		
	}
	
	public static void EscribirFichero(int[] tabla) {
		
		String numeroCadena;
		
		try {
			File fichero = new File(ficheroNumeros);
			FileWriter fw = new FileWriter(fichero);
			for (int i=0; i<tabla.length; i++) {
				numeroCadena = Integer.toString(tabla[i]) + " ";
				fw.write(numeroCadena);
			}
			fw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void LeerFichero(int[] tabla) {
		
		int resultado,
			numeroEntero = 0,
			i;
		
		try {
			File fichero = new File(ficheroNumeros);
			FileReader fr = new FileReader(fichero);
			
			i = 0;
			resultado = fr.read();
			while (resultado != -1) {
				if ((resultado == '\r') || (resultado == '\n')) {
					tabla[i] = numeroEntero;
					i++;
					numeroEntero = 0;
					if (resultado == '\r')
						resultado = fr.read();
 				} else {
 					numeroEntero = (numeroEntero * 10) + (resultado - '0');
 				}
				resultado = fr.read();
				
			}
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		Inicializar(tablaNumeros);
		
		int opcion = 0;
		do {
			System.out.println("=========================================");
			System.out.println("OPCIONES:");
			System.out.println("=========================================");
			System.out.println("Opcion 1: Escribir fichero");
			System.out.println("Opcion 2: Leer fichero");
			System.out.println("Opcion 3: Escribir tabla");
			System.out.println("Opcion 4: Modificar tabla");
			System.out.println("Opcion 5: Actualizar tabla fichero");
			System.out.println("Opcion 6: Salir");
			System.out.println("=========================================");
			System.out.print("Selecciona una opci�n: ");
			
			Scanner numOpcion = new Scanner(System.in);
			opcion = numOpcion.nextInt();
			
			switch (opcion) {
				case 1:
					System.out.println("=========================================");
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 1: Escribir fichero\n");
					EscribirFichero(tablaNumeros);
					System.out.println("-----------------------------------------");
					break;
				case 2:
					System.out.println("=========================================");
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 2: Leer fichero\n");
					LeerFichero(tablaNumeros);
					System.out.println("-----------------------------------------");
					break;
				case 3:
					System.out.println("=========================================");
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 3: Escribir tabla\n");
					for (int i=0; i<tablaNumeros.length; i++) {
						System.out.println(tablaNumeros[i]);
					}
					System.out.println("-----------------------------------------");
					break;
				case 4:
					System.out.println("=========================================");
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 4: Modificar tabla\n");
					System.out.println("-----------------------------------------");
					break;
				case 5:
					System.out.println("=========================================");
					System.out.println("-----------------------------------------");
					System.out.println("Opcion seleccionada 5: Actualizar tabla fichero\n");
					System.out.println("-----------------------------------------");
					break;
				case 6:
					break;
				default:
					System.out.println("\n�No existe esta opci�n escoge otra!\n");
					break;
			}
		} while (opcion !=6);

	}

}
