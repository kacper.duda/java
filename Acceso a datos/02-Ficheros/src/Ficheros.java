import java.io.File;
import java.io.IOException;

public class Ficheros {

	public static void main(String[] args) {
		
		File carpeta = new File("carpeta");
		
		if (carpeta.mkdir() == true) {
			
			File fich1 = new File("carpeta\\Primero.txt"),
				 fich2 = new File("carpeta\\Segundo.txt");
			
			try {
				fich1.createNewFile();
				fich2.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			
		} else {
			System.out.println("Carpeta no creada");
		}
		
	}

}
