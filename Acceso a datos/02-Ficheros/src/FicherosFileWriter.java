import java.io.*;

public class FicherosFileWriter {

	public static void LeerFichero(String nombre) {
		
		try {
			
			// 1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			// 2. Lectura del fichero
			int letra;
			String cadena;
			
			cadena = "";
			letra = fr.read();
			
			while (letra != -1) {
				
				// Tratamiento del car�cter le�do
				cadena += ((char) letra);
				// Leer el siguiente car�cter
				letra = fr.read();
				
			}
			
			// 3. Cerrar el fichero
			fr.close();
			
			// 4. Escribir la cadena por pantalla
			System.out.println("He le�do: " + cadena);
			
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void EscribirFichero() {
		
		try {
			
			// 1. Apertura del fichero	
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);
			
			// 2. Escribir en el fichero
			fw.write("Pepe");
			fw.write("Ana");
			fw.write("Juan");
			
			// 3. Cerrar fichero
			fw.close();
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void main(String[] args) {
		
		// Llamada a EscribirFichero
		EscribirFichero();
		// Llamada a LeerFichero
		LeerFichero("FicheroTexto1.txt");

	}

}
