import java.util.*;
import java.io.*;

public class EscrituraLectura {

	public static void main(String[] args) {
		String[] tablaNombres = {"Juan", "Ana", "Pedro", "Laura", "Luis", "Antonio"};
		String[] otraTabla = new String[10];
		FicheroBuffer ficheroNombres = new FicheroBuffer("Nombres.txt");
		
		ficheroNombres.EscribirTabla(tablaNombres);
		ficheroNombres.LeerTabla(otraTabla);
		
		for(int i=0; i<otraTabla.length; i++) {
			System.out.println("Posicion [" + i + "]: " + otraTabla[i]);
		}
		
		// TABLA PERSONAS
		Persona[] personas = new Persona[3];
		personas[0] = new Persona("Pepe", 20);
		personas[1] = new Persona("Juan", 26);
		personas[2] = new Persona("Luis", 15);
		
		for(int i=0; i<personas.length; i++) {
			System.out.println(personas[i].getNombre() + " " + personas[i].getEdad());
		}
	}
	
}
