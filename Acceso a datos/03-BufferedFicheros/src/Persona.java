import java.util.*;
import java.io.*;

public class Persona {

	private String nombre;
	private int edad;
	
	public Persona () {
		nombre = "";
		edad = 0;
	}
	
	public Persona (String n, int e) {
		nombre = n;
		edad = e;
	}

	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}
	
}
