import java.util.*;
import java.io.*;

public class FicheroBuffer {

	private String nombreFichero;
	
	public FicheroBuffer (String nombre) {
		nombreFichero = nombre;
	}
	
	public void EscribirTabla(String[] tabla) {
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0; i<tabla.length; i++) {
				bw.write(tabla[i]);
				bw.newLine();					 				// ESCRIBE UN NOMBRE POR L�NEA
			}
			bw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}		
	}
	
	public void LeerTabla (String[] tabla) {
		try {
			File fichero = new File(nombreFichero);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			int i;
			
			i = 0;
			linea = br.readLine();
			while(linea != null) {
				tabla[i] = linea;
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}		
	}
	
}
