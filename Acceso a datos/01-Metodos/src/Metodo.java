
public class Metodo {

	public static int sumar(int n1, int n2) {
		int resultado;
		resultado = n1+n2;
		
		return resultado;
	}
	
	public static int sumarNumero(int n1, Numero n) {
		int resultado;
		resultado = n1 + n.DevolverValor();
		n1 = 0;
		n.FijarValor(0);
		
		return resultado;
	}
	
	public static void main(String[] args) {	
		/*
		 * int numero1 = 23, 
		 * 	   numero2 = 41, 
		 *     numero3 = 11, 
		 *     res;
		 * 
		 * res = sumar(numero1, numero2); 
		 * System.out.println("El Resultado es: " + res);
		 * res = sumar(numero1, numero3); 
		 * System.out.println("El Resultado es: " + res);
		 */
		
		int numero1 = 3,
			resultado;
		
		Numero miNumero = new Numero(21),
			   otroNumero = new Numero(33),
			   esteNumero = new Numero(4);
		
		System.out.println("El primero es: " + miNumero.DevolverValor());
		System.out.println("El segundo es: " + otroNumero.DevolverValor());
		
		// resultado = miNumero + 1
		resultado = miNumero.Sumar(1);
		System.out.println("La suma es: " + resultado);
		
		// LLAMADA A M�TODO STATIC
		resultado = Numero.Sumar(21, 33);
		System.out.println("La suma del m�todo static es: " +  resultado);
		
		// LLAMADA CON PAR�METROS DE CLASE
		resultado = sumarNumero(numero1, esteNumero);
		System.out.println("numero1 vale: " + numero1);
		System.out.println("esteNumero vale: " + esteNumero.DevolverValor());
		System.out.println("La suma de la llamada con par�metros es: " +  resultado);
		
	}

}
