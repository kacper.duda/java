
public class Numero {
	
	// ATRIBUTOS
	private int valor;

	// CONSTRUCTOR
	public Numero(int v) {
		valor = v;
	}

	// M�TODOS
	public int Sumar(int v) {
		int resultado;
		resultado = valor + v;
		
		return resultado;
	}
	
	public int DevolverValor() {
		return valor;
	}
	
	public void FijarValor(int nuevo) {
		valor = nuevo;
	}
	
	public static int Sumar(int n1, int n2) {
		int resultado;
		resultado = n1 + n2;
		
		return resultado;
	}
	
}
