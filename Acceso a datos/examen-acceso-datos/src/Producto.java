
public class Producto {
	String idProducto;
	String nombre;
	String idCategoria;
	
	public Producto(String idP, String n, String idC) {
		idProducto = idP;
		nombre = n;
		idCategoria = idC;
	}
	
	public String getIdProducto() {
		return idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public String getIdCategoria() {
		return idCategoria;
	}
	
	public void setIdProducto(String idP) {
		idProducto = idP;
	}
	public void setNombre(String n) {
		nombre = n;
	}
	public void setIdCategoria(String idC) {
		idCategoria = idC;
	}
}
