import java.util.*;
import java.io.*;

public class Principal {

	public static void LeerFicheroCategoria(Categoria[] c) {
		try {
			File fichero = new File("FicheroCategoria.txt");
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			int i;
			String linea, idCategoria, nombre;
			
			idCategoria = null;
			nombre = null;
			i = 0;
			
			linea = br.readLine();
			while(linea != null) {
				idCategoria = linea;
				nombre = br.readLine();
				c[i] = new Categoria(idCategoria,nombre);
				i++;
				linea = br.readLine();
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void LeerFicheroProducto(Producto[] p) {
		try {
			File fichero = new File("FicheroProducto.txt");
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			int i;
			String linea, idProducto, nombre, idCategoria;
			
			idProducto = null;
			nombre = null;
			idCategoria = null;
			i = 0;
			
			linea = br.readLine();
			while(linea != null) {
				idProducto = linea;
				nombre = br.readLine();
				idCategoria = br.readLine();
				p[i] = new Producto(idProducto,nombre,idCategoria);
				i++;
				linea = br.readLine();
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void EscribirResumen(Categoria[] c, Producto[] p) {
		int np;
		String idCategoria, idProductoCategoria;
		
		for (int i=0; i<c.length; i++) {
			np = 0;
			idCategoria = c[i].getIdCategoria();
			for (int j=0; j<p.length; j++) {
				idProductoCategoria = p[i].getIdCategoria();
				if(idCategoria.equals(idProductoCategoria));
					np++;
			}
			System.out.println("Categoria " + c[i].getNombre() + ":" + np);
			System.out.println(c + " " + p);
		}
	}
	
	public static void main(String[] args) {
		Categoria[] categoria = new Categoria[3];
		Producto[] producto = new Producto[3];
		
		LeerFicheroCategoria(categoria);
		LeerFicheroProducto(producto);
		EscribirResumen(categoria, producto);
	}

}
