
public class Categoria {
	private String idCategoria;
	private String nombre;
	
	public Categoria(String idC, String n) {
		idCategoria = idC;
		nombre = n;
	}
	
	public String getIdCategoria() {
		return idCategoria;
	}
	public String getNombre() {
		return nombre;
	}
	public void setIdCategoria(String idC) {
		idCategoria = idC;
	}
	public void setNombre(String n) {
		nombre = n;
	}
}
