import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Buscar {

	public static void LeerDatos(String n) {
		try {
			File fichero = new File("Nombres.txt");
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			int i;
			String linea;
			
			i = 1;
			linea = null;
			
			linea = br.readLine();
			while(linea != null) {
				if (n.equals(linea)) {
					System.out.println("Se encuentra en la linea: " + i);
					break;
				} else {
					i++;
					linea = br.readLine();
				}
			}
			if(linea == null)
				System.out.println("No se ha encontrado el nombre en el fichero.");
			br.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		String nombre;
		
		System.out.println("Introduce un nombre: ");
		
		Scanner teclado = new Scanner(System.in);
		nombre = teclado.nextLine();
		
		LeerDatos(nombre);
	}

}
