import java.util.*;
import java.io.*;

public class PrincipalLista10 {
	
	public static int escribirMenu()
	{
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Menu de opciones");
		System.out.println(" 1. Leer fichero en la tabla");
		System.out.println(" 2. Escribir tabla en el fichero");
		System.out.println(" 3. Escribir tabla pantalla");
		System.out.println(" 4. Modificar tabla");
		System.out.println(" 5. Actualizar la tabla sobre el fichero");
		System.out.println(" 6. Salir");
		
		return entrada.nextInt();
	}
	
	public static void EscribirTabla(int[] tabla)
	{
		for(int i=0;i<tabla.length;i++)
		{
			System.out.print(tabla[i] + " ");
		}
	}
	
	public static void EscribirTablaFichero(String nombre,int[] tabla)
	{
		String numeroCadena;
		try
		{
			File fichero = new File(nombre);
			FileWriter file = new FileWriter(fichero);
			for(int i=0;i<tabla.length;i++)
			{
				numeroCadena = Integer.toString(tabla[i]);
				file.write(numeroCadena);
				file.write(' ');
			}
			file.close();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}				
	}
	
	public static void EscribirTablaFicheroSaltos(String nombre,int[] tabla)
	{
		String numeroCadena;
		try
		{
			File fichero = new File(nombre);
			FileWriter file = new FileWriter(fichero);
			for(int i=0;i<tabla.length;i++)
			{
				numeroCadena = Integer.toString(tabla[i]);
				file.write(numeroCadena);
				file.write('\n');
			}
			file.close();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}				
	}
	
	public static void LeerTablaFichero(String nombre,int[] tabla)
	{
		int resultado,
		    numeroEntero,
		    i;
		String numeroCadena;
		
		try
		{
			File fichero = new File(nombre);
			FileReader file = new FileReader(fichero);
			
			i = 0; 
			numeroCadena = "";
			resultado = file.read();
			while (resultado != -1)
			{
				if (resultado == ' ')
				{
					numeroEntero = Integer.parseInt(numeroCadena);
					tabla[i] = numeroEntero;
					i++;
					numeroCadena = "";
				}
				else
					numeroCadena += (char)resultado;
			    resultado = file.read();
			}
			file.close();
		}
		catch(FileNotFoundException ex)
		{
			ex.printStackTrace();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		
	}
	public static void LeerTablaFicheroSaltos(String nombre,int[] tabla)
	{
		int resultado,
		    numeroEntero,
		    i;
		String numeroCadena;
		
		try
		{
			File fichero = new File(nombre);
			FileReader file = new FileReader(fichero);
			
			i = 0; 
			numeroCadena = "";
			resultado = file.read();
			while (resultado != -1)
			{
				if ((resultado == '\r') || (resultado == '\n')) 
				{
					numeroEntero = Integer.parseInt(numeroCadena);
					tabla[i] = numeroEntero;
					i++;
					numeroCadena = "";
					if (resultado=='\r')
						resultado = file.read();
				}
				else
					numeroCadena += (char)resultado;
			    resultado = file.read();
			}
			file.close();
		}
		catch(FileNotFoundException ex)
		{
			ex.printStackTrace();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		
	}

	public static void LeerTablaFicheroV2(String nombre,int[] tabla)
	{
		int resultado,
		    numeroEntero,
		    i;
		
		try
		{
			File fichero = new File(nombre);
			FileReader file = new FileReader(fichero);
			i = 0; 
			numeroEntero = 0;
			resultado = file.read();
			while (resultado != -1)
			{
				if (resultado == ' ')
				{
					tabla[i] = numeroEntero;
					i++;
					numeroEntero = 0;
				}
				else
					numeroEntero = (numeroEntero*10) + (resultado-'0');
			    resultado = file.read();
			}
			file.close();
		}
		catch(FileNotFoundException ex)
		{
			ex.printStackTrace();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	
	
	public static void main(String[] args)
	{
		/*
		 * FicheroNumeros fn = new FicheroNumeros("Numeros1.txt"); TablaEnteros
		 * miTablaNumeros = new TablaEnteros(10);
		 */
		
		int[] tablaNumeros = new int[10];
		
		int opcion;
		do
		{
			opcion = escribirMenu();
			switch(opcion)
			{
			    case 1:
				    // Leer fichero y guardar en tabla
				    // fn.LeerTabla(miTablaNumeros.getTabla());
			    	// fn.LeerTablaV2(miTablaNumeros.getTabla());
			    	// LeerTablaFichero("Numeros1.txt",tablaNumeros);
			    	// LeerTablaFicheroV2("Numeros1.txt",tablaNumeros);
			    	LeerTablaFicheroSaltos("Numeros2.txt",tablaNumeros);
				    break;
			    case 2: 
			    	// Escribir la tabla en el fichero
			    	// EscribirTablaFichero("Numeros1.txt",tablaNumeros);
			    	EscribirTablaFicheroSaltos("Numeros2.txt",tablaNumeros);
			    	break;
			    case 3:
			    	// Escribir tabla por pantalla
			    	EscribirTabla(tablaNumeros);
			    	break;
			    case 4:
			    	// Modificar la tabla
			    	Scanner entrada = new Scanner(System.in);
			    	int posicion, nuevoValor;
			    	
			    	System.out.println("Introduce la posici�n: ");
			    	posicion = entrada.nextInt();
			    	System.out.println("Introduce el nuevo valor: ");
			    	nuevoValor = entrada.nextInt();
			    	
			    	tablaNumeros[posicion] = nuevoValor;
			    	
			    	// miTablaNumeros.getTabla()[posicion] = nuevoValor;
			        break;
			    case 5:
			    	// Actualizar el contenido de la tabla sobre el fichero
			    	// fn.EscribirTabla(miTablaNumeros.getTabla());
			    	break;
			}
		}while (opcion!=6);
	}

}
