package ej1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Principal {
	public static int imprimirMenu(int opcion) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Selecciona una opcion: \n" 
						+ "1: IMPORTE DE FACTURA. \n" 
						+ "2: FACTURA (MAS ARTICULOS) \n"
						+ "3: SALIR \n");
		System.out.print("OPCION: ");
		return opcion = teclado.nextInt();

	}

	public static void leerDatos(Factura[] factura, Articulo[] articulo) {
		try {
			File fichero = new File("src//ej1//facturas.txt");
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			String linea, nombreArt;
			int i = 0, c = 0, id, nArt, cant, p;

			linea = br.readLine();

			while (linea != null) {
				id = Integer.parseInt(linea);
				linea = br.readLine();
				nArt = Integer.parseInt(linea);
				cant = nArt;
				for (int j = 0; j < cant; j++) {
					nombreArt = (linea = br.readLine());
					linea = br.readLine();
					p = Integer.parseInt(linea);
					articulo[c] = new Articulo(nombreArt, p);
					c++;
				}
				factura[i] = new Factura(id, nArt);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {

		Factura[] factura = new Factura[3];
		Articulo[] articulo = new Articulo[9];

		leerDatos(factura, articulo);

		int opcion = 0;

		do {
			switch (imprimirMenu(opcion)) {
			case 1:
				int total = 0, id = 0, x = 0, cnt = 0;
				for (int i = 0; i < factura.length; i++) {
					id = factura[i].getIdFactura();
					x += factura[i].getNArticulos();
					while (cnt < x) {
						total += articulo[cnt].getPrecio();
						cnt++;
					}
					System.out.println("La factura " + id + " tiene un total de " + total + "\n");
					total = 0;
				}
				break;
			case 2:
				int min, id2;
				min = -1;
				id2 = 0;
				for (int i = 0; i < factura.length; i++) {
					if (factura[i].getNArticulos() > min) {
						id2 = factura[i].getIdFactura();
						min = factura[i].getNArticulos();
					}
				}

				System.out.println(
						"La factura con mas articulos es la " + id2 + ", contiene " + min + " articulos\n");

				break;
			case 3:
				System.out.println("Usted ha seleccionado la opcion salir.");
				System.exit(0);
				break;

			default:
				System.out.println("\nSELECCIONE UNA OPCION VALIDA.\n");
				System.out.println();
			}
		} while (opcion != 3);
	}
}
