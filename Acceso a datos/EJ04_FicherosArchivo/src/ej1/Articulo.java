package ej1;

public class Articulo {
	private String nombreArticulo;
	private int precio;
	
	public Articulo (String nA, int p) {
		nombreArticulo = nA;
		precio = p;
	}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nA) {
		nombreArticulo = nA;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(int p) {
		precio = p;
	}
}
