package ej1;

public class Factura {
	private int idFactura;
	private int nArticulos;
	
	public Factura (int idF, int nA) {
		idFactura = idF;
		nArticulos = nA;
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idF) {
		idFactura = idF;
	}

	public int getNArticulos() {
		return nArticulos;
	}

	public void setNArticulos(int nA) {
		nArticulos = nA;
	}
}
