package ej2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class ej2 {
	public static void SacarVocales(String c, String vm, int[] cnt) {
		for (int i = 0; i < c.length(); i++) {
			for (int j = 0; j < vm.length(); j++) {
				if (c.charAt(i) == vm.charAt(j)) {
					cnt[j]++;
				}
			}
		}

		for (int i = 0; i < vm.length(); i++) {
			System.out.println("La vocal " + vm.charAt(i) + " aparece: " + cnt[i] + " veces");
		}
	}
	public static void EscribirFichero(String c, String vm, String vM, int[] cntm, int[] cntM) {
		try {
			File fichero = new File("src//ej2//vocales.txt");
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("En la frase: " + c);
			bw.write("\n\n");
			for (int i = 0; i < vm.length(); i++) {
				bw.write("La letra " + vm.charAt(i) + " aparece: " + cntm[i] + " veces\n");
			}
			bw.write("\n");
			for (int i = 0; i < vM.length(); i++) {
				bw.write("La letra " + vM.charAt(i) + " aparece: " + cntM[i] + " veces\n");
			}
			bw.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		String c = "", 
			   vm = "aeiou", 
			   vM = "AEIOU";
		int cntm[] = { 0, 0, 0, 0, 0 },
			cntM[] = { 0, 0, 0, 0, 0 };

		System.out.print("Introduce una frase: ");
		c = teclado.nextLine();
		System.out.println("En la frase: " + c + "\n");

		SacarVocales(c, vm, cntm);
		SacarVocales(c, vM, cntM);
		EscribirFichero(c, vm, vM, cntm, cntM);
	}
}
