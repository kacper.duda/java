import java.io.*;
import java.util.*;

public class ExamenEj4 {

	public static void CrearEstructura(int n1, int n2) {
		
		File carpeta,		// CARPETA CREADA
			 fichero;		// FICHEROS EN LA CARPETA
		
		try {
			carpeta = new File(Integer.toString(n2));
			if (carpeta.mkdir() == true) {
				for(int i=n1; i<=n2; i++) {
					fichero = new File(Integer.toString(n2) + "\\" + Integer.toString(i) + ".num");
					fichero.createNewFile();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		int n1, n2;
		
		Scanner num = new Scanner(System.in);
		System.out.println("Introduce el primer n�mero: ");
		n1 = num.nextInt();
		System.out.println("\nIntroduce el segundo n�mero: ");
		n2 = num.nextInt();
		
		CrearEstructura(n1,n2);
		
	}

}
