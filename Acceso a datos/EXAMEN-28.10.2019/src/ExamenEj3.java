import java.io.*;
import java.util.*;

public class ExamenEj3 {
	
	public static void LeerFichero(String fNombre, String[] tNombre, int[] tDni) {
		
		int resultado, 			// VARIABLE PARA LEER
			i,					// �NDICE DE TABLAS
			nEmpleados;			// N� EMPLEADOS
		String valorCadena;		// CONVERTIR DATOS LE�DOS
		boolean numero,			// INDICA SI LEE N�MERO DEL FICHERO
				primero;		// INDICA SI LEE EL PRIMER N�MERO DEL FICHERO
		
		try {
			File fichero = new File(fNombre);
			FileReader fr = new FileReader(fichero);
			
			numero = true;
			primero = true;
			i = 0;
			valorCadena = "";
			resultado = fr.read();
			
			while (resultado != -1) {
				if (resultado != '-')
					valorCadena += (char)resultado;
				else {
					if (numero == true) {
						if (primero == true) {
							nEmpleados = Integer.parseInt(valorCadena);
							primero = false;
						} else {
							tDni[i] = Integer.parseInt(valorCadena);
							i++;
						}
						numero = false;
					} else {
						// numero = false --> Leemos un nombre
						tNombre[i] = valorCadena;
						numero = true;
					}
					valorCadena = "";
				}
				resultado = fr.read();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void EscribirPantalla(String[] tNombre, int[] tDni) {
		
		System.out.println("El contenido es: ");
		for(int i=0; i<tNombre.length; i++) {
			System.out.println(tNombre[i] + " - " + tDni[i]);
		}
		
	}
	
	public static void EscribirMayorDNI(String[] tNombre, int[] tDni) {
		
		int DNIMaximo,
			posMax;
		
		DNIMaximo =  -1000000000;
		posMax = 0;
		
		for(int i=0; i<tDni.length; i++) {
			if (tDni[i] > DNIMaximo) {
				DNIMaximo = tDni[i];
				posMax = i;
			}
		}
		System.out.println("El nombre del DNI mayor es: " + tNombre[posMax]);
		
	}
	
	public static void EscribirMenorDNI(String[] tNombre, int[] tDni) {
		
		int DNIMinimo,
			posMin;
		
		DNIMinimo =  1000000000;
		posMin = 0;
		
		for(int i=0; i<tDni.length; i++) {
			if ((tDni[i] != 0) && (tDni[i] < DNIMinimo)) {
				DNIMinimo = tDni[i];
				posMin = i;
			}
		}
		System.out.println("El nombre del DNI menor es: " + tNombre[posMin]);
		
	}
	
	public static void main(String[] args) {
		
		String[] tNombre = new String[10];
		int[] tDni = new int[10];
		String fNombre = "Empleados.txt";
		
		LeerFichero(fNombre, tNombre, tDni);
		EscribirPantalla(tNombre, tDni);
		EscribirMayorDNI(tNombre, tDni);
		EscribirMenorDNI(tNombre, tDni);
		
	}

}
