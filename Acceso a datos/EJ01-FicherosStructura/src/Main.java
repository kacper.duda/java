import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		
		String[] rutas = { "nombre", "nombre\\bin", "nombre\\bin\\bytecode", "nombre\\bin\\objetos", "nombre\\src", "nombre\\src\\clases", "nombre\\doc", "nombre\\doc\\html", "nombre\\doc\\pdf" };
		
		for(int i=0; i<rutas.length; i++) {
			
			File carpeta = new File(rutas[i]);
			
			if (carpeta.mkdir() == true) {
				System.out.println(rutas[i] + " - Carpeta creada.");
			} else if (carpeta.exists() == true) {
				System.out.println(rutas[i] + " - La carpeta ya existe.");
			} else {
				System.out.println(rutas[i] + " - No se ha podido crear.");
			}
			
		}

	}

}
