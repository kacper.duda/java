import java.io.*;

public class FileWriterReader {

	public static void LeerFichero(String nombre, String[] nombres) {
		
		try {
			
			// 1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			// 2. Lectura del fichero
			int letra;
			int pos = 0;
			String cadena;
			
			cadena = "";
			letra = fr.read();
			
			while (letra != -1) {
				
				if (letra == '\n') {
					nombres[pos] = cadena;
					cadena = "";
					pos++;
				} else {
					// Tratamiento del car�cter le�do
					cadena += ((char) letra);
				}
				
				// Leer el siguiente car�cter
				letra = fr.read();
				
			}
			
			// 3. Cerrar el fichero
			fr.close();
			
			// 4. Escribir la cadena por pantalla
			System.out.println("He le�do: " + cadena);
			
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void EscribirFichero(String nombre, String[] nombres) {
		
		try {
			
			// 1. Apertura del fichero	
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			
			// 2. Escribir en el fichero
			for (int i=0; i<nombres.length; i++) {
				fw.write(nombres[i] + "\n");
			}

			// 3. Cerrar fichero
			fw.close();
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void LeerFicheroV2(String nombre, String[] nombres) {
		
		try {
			
			// 1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			// 2. Lectura del fichero
			int letra;
			int pos = 0;
			String cadena;
			
			cadena = "";
			letra = fr.read();
			
			while (letra != -1) {
				
				if (letra == ';') {
					nombres[pos] = cadena;
					cadena = "";
					pos++;
				} else {
					// Tratamiento del car�cter le�do
					cadena += ((char) letra);
				}
				
				// Leer el siguiente car�cter
				letra = fr.read();
				
			}
			
			// 3. Cerrar el fichero
			fr.close();
			
			// 4. Escribir la cadena por pantalla
			System.out.println("He le�do: " + cadena);
			
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void EscribirFicheroV2(String nombre, String[] nombres) {
		
		try {
			
			// 1. Apertura del fichero	
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			
			// 2. Escribir en el fichero
			for (int i=0; i<nombres.length; i++) {
				fw.write(nombres[i] + ";");
			}

			// 3. Cerrar fichero
			fw.close();
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		
	}
	
	public static void InicializarTabla(String[] nombres) {
		for (int i=0; i<nombres.length; i++) {
			nombres[i] = "";
		}
	}
	
	public static void EscribirTabla(String[] nombres) {
		System.out.println("Contenido de la tabla: ");
		for (int i=0; i<nombres.length; i++) {
			System.out.println(nombres[i]);
		}
	}
	
	public static void main(String[] args) {
		
		String[] listaNombres = { "Pepe", "Laura", "Juan", "Ana" };
 		
		// Llamada a EscribirFichero
		EscribirFichero("FicheroTexto1.txt", listaNombres);
		EscribirFicheroV2("FicheroTexto1V2.txt", listaNombres);
		
		// Inicializar tabla
		InicializarTabla(listaNombres);
		EscribirTabla(listaNombres);
		
		// Llamada a LeerFichero
		LeerFichero("FicheroTexto1.txt", listaNombres);
		LeerFicheroV2("FicheroTexto1V2.txt", listaNombres);
		EscribirTabla(listaNombres);

	}

}
