/* 
 * TIPOS DE VISIBILIDAD DE VARIABLES: 
 * 
 * 		' public ' 		-> visibles para todas las dem�s clases, es inseguro, no es com�n utilizarlo.
 * 		' protected ' 	-> se utiliza en herencia, lo llevan las clases PADRE y solo es visible para las clases HIJAS.
 * 		' private ' 	-> se utiliza en las clases HIJAS para los atributos para mantener los get, set y toString p�blicos
 * 
 */



public class Figura {
	
	protected String color;
	protected char tama�o; // 'G' = grande && 'M' = mediano && 'P' = peque�o
	
	public Figura(String color, char tama�o) {
		super();
		this.color = color;
		this.tama�o = tama�o;
	}

	protected String getColor() {
		return color;
	}

	protected void setColor(String color) {
		this.color = color;
	}

	protected char getTama�o() {
		return tama�o;
	}

	protected void setTama�o(char tama�o) {
		this.tama�o = tama�o;
	}

	@Override
	public String toString() {
		return "Figura [color=" + color + ", tama�o=" + tama�o + "]";
	}

}
