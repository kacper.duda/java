
public class Circulo extends Figura {
	
	private double radio;

	public Circulo(String color, char tama�o, double radio) {
		super(color, tama�o);
		this.radio = radio;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	@Override
	public String toString() {
		return "Circulo [radio=" + radio + "]";
	}
	
	// Metodo calcularArea
	public static double calcularArea (double radio) {
		double area;
		area = Math.PI * Math.pow(radio, 2);
		return area;
	}
}
