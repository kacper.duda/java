import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// MENU 
		int n=0;
		do {
			System.out.println("\n");
			System.out.println("------------------------------------");
			System.out.println("|	Men� Calcular Area	   |");
			System.out.println("------------------------------------");
			System.out.println("1. C�rculo");
			System.out.println("2. Cuadrado");
			System.out.println("3. Triangulo");
			System.out.println("4. Rombo");
			System.out.println("5. Trapecio");
			System.out.println("6. Rect�ngulo");
			System.out.println("7. Para salir");
			System.out.println("\n");
			System.out.println("Elija una opci�n: ");
			
			// Pide datos
			@SuppressWarnings("resource") // QUITA LOS WARNING DE UN RECURSO QUE ESTE DEBAJO
			Scanner teclado = new Scanner(System.in);
			n = teclado.nextInt();
			
			System.out.println("\n");
			
			switch (n) {
            	case 1: 
            		System.out.println("Calcular area del C�rculo: \n");
            		System.out.print("\t Introduzca el radio: \n");
            		// pide el dato radio
            		double circuloRadio = teclado.nextDouble();
            		System.out.print("El area del circulo es: " + Circulo.calcularArea(circuloRadio));
            		System.out.println("\n");
            		break;
            	case 2: 
            		System.out.println("Calcular area del Cuadrado: \n");
            		System.out.print("\t Introduzca el lado: \n");
            		// pide el dato lado
            		double cuadradoLado = teclado.nextDouble();
            		System.out.print("El area del cuadrado es: " + Cuadrado.calcularArea(cuadradoLado));
            		System.out.println("\n");
            		break;
            	case 3: 
            		System.out.println("Calcular area del Tri�ngulo: \n");
            		System.out.print("\t Introduzca la base: \n");
            		// pide el dato base
            		double trianguloBase = teclado.nextDouble();
            		System.out.print("\t Introduzca la altura: \n");
            		// pide el dato altura
            		double trianguloAltura = teclado.nextDouble();
            		System.out.print("El area del triangulo es: " + Triangulo.calcularArea(trianguloBase, trianguloAltura));
            		System.out.println("\n");
            		break;
            	case 4: 
            		System.out.println("Calcular area del Rombo: \n");
            		System.out.print("\t Introduzca la diagonal mayor: \n");
            		// pide el dato dMayor
            		double romboDMayor = teclado.nextDouble();
            		System.out.print("\t Introduzca la diagonal menor: \n");
            		// pide el dato dMenor
            		double romboDMenor = teclado.nextDouble();
            		System.out.print("El area del rombo es: " + Rombo.calcularArea(romboDMayor, romboDMenor));
            		System.out.println("\n");
            		break;
            	case 5: 
            		System.out.println("Calcular area del Trapecio: \n");
            		System.out.print("\t Introduzca la base mayor: \n");
            		// pide el dato bMayor
            		double trapecioBMayor = teclado.nextDouble();
            		System.out.print("\t Introduzca la base menor: \n");
            		// pide el dato bMenor
            		double trapecioBMenor = teclado.nextDouble();
            		// pide el dato altura
            		System.out.print("\t Introduzca la altura: \n");
            		double trapecioAltura = teclado.nextDouble();
            		System.out.print("El area del trapecio es: " + Trapecio.calcularArea(trapecioBMayor, trapecioBMenor, trapecioAltura));
            		System.out.println("\n");
            		break;
            	case 6: 
            		System.out.println("Calcular area del Rect�ngulo: \n");
            		System.out.print("\t Introduzca la Base: \n");
            		// pide el dato base
            		double rectanguloBase = teclado.nextDouble();
            		System.out.print("\t Introduzca la altura: \n");
            		// pide el dato altura
            		double rectanguloAltura = teclado.nextDouble();
            		System.out.print("El area del rectangulo es: " + Rectangulo.calcularArea(rectanguloBase, rectanguloAltura));
            		System.out.println("\n");
            		break;
            	case 7: 
            		System.out.println("Hasta luego!\n ");
            		break;
            	default: 
            		System.out.println("Eleccion no v�lida\n ");
            		break;

        }
		
		}while(n!=7);
		
		System.out.println("\n");
		
	}

}
