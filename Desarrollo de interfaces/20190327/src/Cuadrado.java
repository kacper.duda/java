
public class Cuadrado extends Figura {
	
	double lado;

	public Cuadrado(String color, char tama�o, double lado) {
		super(color, tama�o);
		this.lado = lado;
	}

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	@Override
	public String toString() {
		return "Cuadrado [lado=" + lado + "]";
	}
	
	// Metodo calcularArea
	public static double calcularArea (double lado) {
		double area;
		area = lado * lado;
		return area;
	}
	
}
