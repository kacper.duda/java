
public class Rombo extends Figura {

	double dMayor;
	double dMenor;
	
	public Rombo(String color, char tama�o, double dMayor, double dMenor) {
		super(color, tama�o);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}

	public double getdMayor() {
		return dMayor;
	}

	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}

	public double getdMenor() {
		return dMenor;
	}

	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}

	@Override
	public String toString() {
		return "Rombo [dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}
	
	// Metodo calcularArea
	public static double calcularArea (double dMayor, double dMenor) {
		double area;
		area = (dMenor * dMayor) / 2;
		return area;
	}
	
}
