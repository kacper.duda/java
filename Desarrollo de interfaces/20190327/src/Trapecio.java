
public class Trapecio extends Figura {

	double bMayor;
	double bMenor;
	double altura;
	
	public Trapecio(String color, char tama�o, double bMayor, double bMenor, double altura) {
		super(color, tama�o);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}

	public double getbMayor() {
		return bMayor;
	}

	public void setbMayor(double bMayor) {
		this.bMayor = bMayor;
	}

	public double getbMenor() {
		return bMenor;
	}

	public void setbMenor(double bMenor) {
		this.bMenor = bMenor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Trapecio [bMayor=" + bMayor + ", bMenor=" + bMenor + ", altura=" + altura + "]";
	}
	
	// Metodo calcularArea
	public static double calcularArea (double bMayor, double bMenor, double altura) {
		double area;
		area = ((bMayor + bMenor) / 2) * altura;
		return area;
	}
	
}
