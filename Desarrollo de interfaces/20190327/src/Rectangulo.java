
public class Rectangulo extends Figura {

	double base;
	double altura;
	
	public Rectangulo(String color, char tama�o, double base, double altura) {
		super(color, tama�o);
		this.base = base;
		this.altura = altura;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Rectangulo [base=" + base + ", altura=" + altura + "]";
	}
	
	// Metodo calcularArea
	public static double calcularArea (double base, double altura) {
		double area;
		area = base * altura;
		return area;
	}
	
}
