import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuPrincipal {

	private JFrame Ventana;
	private JTextField txtRadio_Circulo;
	private JTextField txtLado_Cuadrado;
	private JTextField txtBase_Triangulo;
	private JTextField txtAltura_Triangulo;
	private JTextField txtDMayor_Rombo;
	private JTextField txtDMenor_Rombo;
	private JTextField txtBMenor_Trapecio;
	private JTextField txtBMayor_Trapecio;
	private JTextField txtAltura_Trapecio;
	private JTextField txtBase_Rectangulo;
	private JTextField txtAltura_Rectangulo;
	double area;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal window = new MenuPrincipal();
					window.Ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Ventana = new JFrame();
		Ventana.setTitle("Calcular Area");
		Ventana.setBounds(100, 100, 882, 605);
		Ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Ventana.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 866, 566);
		panel.setBackground(Color.WHITE);
		Ventana.getContentPane().add(panel);
		panel.setLayout(null);
		
		
		JLabel lblTitulo = new JLabel("CALCULAR AREA");
		lblTitulo.setFont(new Font("Segoe Script", Font.BOLD | Font.ITALIC, 35));
		lblTitulo.setBounds(10, 11, 351, 64);
		panel.add(lblTitulo);
		
		JLabel lblFigura = new JLabel("Selecciona una figura para calcular el \u00C1REA: ");
		lblFigura.setFont(new Font("Segoe Script", Font.BOLD, 20));
		lblFigura.setBounds(20, 86, 516, 37);
		panel.add(lblFigura);
		
		JComboBox DesplegableElegirFigura = new JComboBox();
		DesplegableElegirFigura.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 18));
		DesplegableElegirFigura.setModel(new DefaultComboBoxModel(new String[] {"SELECCIONA UNA FIGURA", "C\u00EDrculo", "Cuadrado", "Triangulo", "Rombo", "Trapecio", "Rect\u00E1ngulo"}));
		DesplegableElegirFigura.setBounds(30, 123, 327, 46);
		panel.add(DesplegableElegirFigura);
		
		JButton Salir = new JButton("SALIR");
		Salir.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		Salir.setBounds(642, 11, 214, 46);
		panel.add(Salir);
		
		//CIRCULO
		JPanel Panel_Circulo = new JPanel();
		Panel_Circulo.setBounds(34, 205, 795, 320);
		panel.add(Panel_Circulo);
		Panel_Circulo.setLayout(null);
		
		JLabel lblRadio_Circulo = new JLabel("Introduce el RADIO del CIRCULO : ");
		lblRadio_Circulo.setBounds(10, 11, 775, 44);
		Panel_Circulo.add(lblRadio_Circulo);
		lblRadio_Circulo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtRadio_Circulo = new JTextField();
		txtRadio_Circulo.setBounds(10, 55, 328, 32);
		Panel_Circulo.add(txtRadio_Circulo);
		txtRadio_Circulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtRadio_Circulo.setColumns(10);
		
		JButton btnCalcular_Circulo = new JButton("CALCULAR AREA");
		btnCalcular_Circulo.setBounds(59, 94, 214, 46);
		Panel_Circulo.add(btnCalcular_Circulo);
		btnCalcular_Circulo.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Circulo.setVisible(false);

		
		//CUADRADO
		JPanel Panel_Cuadrado = new JPanel();
		Panel_Cuadrado.setBounds(34, 205, 795, 320);
		panel.add(Panel_Cuadrado);
		Panel_Cuadrado.setLayout(null);
		
		JLabel lblLado_Cuadrado = new JLabel("Introduce el LADO del CUADRADO : ");
		lblLado_Cuadrado.setBounds(10, 11, 775, 44);
		Panel_Cuadrado.add(lblLado_Cuadrado);
		lblLado_Cuadrado.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtLado_Cuadrado = new JTextField();
		txtLado_Cuadrado.setBounds(10, 55, 328, 32);
		Panel_Cuadrado.add(txtLado_Cuadrado);
		txtLado_Cuadrado.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtLado_Cuadrado.setColumns(10);
		
		JButton btnCalcular_Cuadrado = new JButton("CALCULAR AREA");
		btnCalcular_Cuadrado.setBounds(59, 94, 214, 46);
		Panel_Cuadrado.add(btnCalcular_Cuadrado);
		btnCalcular_Cuadrado.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Cuadrado.setVisible(false);
		
		//TRIANGULO
		JPanel Panel_Triangulo = new JPanel();
		Panel_Triangulo.setBounds(34, 205, 795, 320);
		panel.add(Panel_Triangulo);
		Panel_Triangulo.setLayout(null);
		
		JLabel lblBase_Triangulo = new JLabel("Introduce la BASE del TRIANGULO : ");
		lblBase_Triangulo.setBounds(10, 11, 775, 44);
		Panel_Triangulo.add(lblBase_Triangulo);
		lblBase_Triangulo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtBase_Triangulo = new JTextField();
		txtBase_Triangulo.setBounds(10, 55, 328, 32);
		Panel_Triangulo.add(txtBase_Triangulo);
		txtBase_Triangulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtBase_Triangulo.setColumns(10);
		
		JLabel lblAltura_Triangulo = new JLabel("Introduce la ALTURA del TRIANGULO : ");
		lblAltura_Triangulo.setBounds(10, 97, 775, 44);
		Panel_Triangulo.add(lblAltura_Triangulo);
		lblAltura_Triangulo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtAltura_Triangulo = new JTextField();
		txtAltura_Triangulo.setBounds(10, 141, 328, 32);
		Panel_Triangulo.add(txtAltura_Triangulo);
		txtAltura_Triangulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAltura_Triangulo.setColumns(10);
		
		JButton btnCalcular_Triangulo = new JButton("CALCULAR AREA");
		btnCalcular_Triangulo.setBounds(51, 184, 214, 46);
		Panel_Triangulo.add(btnCalcular_Triangulo);
		btnCalcular_Triangulo.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Triangulo.setVisible(false);
		
		//ROMBO
		JPanel Panel_Rombo = new JPanel();
		Panel_Rombo.setBounds(34, 205, 795, 320);
		panel.add(Panel_Rombo);
		Panel_Rombo.setLayout(null);
		
		JLabel lblDMenor_Rombo = new JLabel("Introduce la DIAGONAL MENOR del ROMBO : ");
		lblDMenor_Rombo.setBounds(10, 11, 775, 44);
		Panel_Rombo.add(lblDMenor_Rombo);
		lblDMenor_Rombo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtDMenor_Rombo = new JTextField();
		txtDMenor_Rombo.setBounds(10, 55, 328, 32);
		Panel_Rombo.add(txtDMenor_Rombo);
		txtDMenor_Rombo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtDMenor_Rombo.setColumns(10);
		
		JLabel lblDMayor_Rombo = new JLabel("Introduce la DIAGONAL MAYOR del ROMBO : ");
		lblDMayor_Rombo.setBounds(10, 97, 775, 44);
		Panel_Rombo.add(lblDMayor_Rombo);
		lblDMayor_Rombo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtDMayor_Rombo = new JTextField();
		txtDMayor_Rombo.setBounds(10, 141, 328, 32);
		Panel_Rombo.add(txtDMayor_Rombo);
		txtDMayor_Rombo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtDMayor_Rombo.setColumns(10);
		
		JButton btnCalcular_Rombo = new JButton("CALCULAR AREA");
		btnCalcular_Rombo.setBounds(51, 184, 214, 46);
		Panel_Rombo.add(btnCalcular_Rombo);
		btnCalcular_Rombo.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Rombo.setVisible(false);
		
		//TRAPECIO
		JPanel Panel_Trapecio = new JPanel();
		Panel_Trapecio.setBounds(34, 205, 795, 320);
		panel.add(Panel_Trapecio);
		Panel_Trapecio.setLayout(null);
		
		JLabel lblBMenor_Trapecio = new JLabel("Introduce la BASE MENOR del TRAPECIO : ");
		lblBMenor_Trapecio.setBounds(10, 11, 775, 44);
		Panel_Trapecio.add(lblBMenor_Trapecio);
		lblBMenor_Trapecio.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtBMenor_Trapecio = new JTextField();
		txtBMenor_Trapecio.setBounds(10, 55, 328, 32);
		Panel_Trapecio.add(txtBMenor_Trapecio);
		txtBMenor_Trapecio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtBMenor_Trapecio.setColumns(10);
		
		JLabel lblBMayor_Trapecio = new JLabel("Introduce la BASE MAYOR del ROMBO : ");
		lblBMayor_Trapecio.setBounds(10, 97, 775, 44);
		Panel_Trapecio.add(lblBMayor_Trapecio);
		lblBMayor_Trapecio.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtBMayor_Trapecio = new JTextField();
		txtBMayor_Trapecio.setBounds(10, 141, 328, 32);
		Panel_Trapecio.add(txtBMayor_Trapecio);
		txtBMayor_Trapecio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtBMayor_Trapecio.setColumns(10);
		
		JLabel lblAltura_Trapecio = new JLabel("Introduce la ALTURA del TRAPECIO : ");
		lblAltura_Trapecio.setBounds(10, 184, 775, 44);
		Panel_Trapecio.add(lblAltura_Trapecio);
		lblAltura_Trapecio.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtAltura_Trapecio = new JTextField();
		txtAltura_Trapecio.setBounds(10, 228, 328, 32);
		Panel_Trapecio.add(txtAltura_Trapecio);
		txtAltura_Trapecio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAltura_Trapecio.setColumns(10);
		
		JButton btnCalcular_Trapecio = new JButton("CALCULAR AREA");
		btnCalcular_Trapecio.setBounds(51, 271, 214, 46);
		Panel_Trapecio.add(btnCalcular_Trapecio);
		btnCalcular_Trapecio.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Trapecio.setVisible(false);
		
		//RECTANGULO
		JPanel Panel_Rectangulo = new JPanel();
		Panel_Rectangulo.setBounds(34, 205, 795, 320);
		panel.add(Panel_Rectangulo);
		Panel_Rectangulo.setLayout(null);
		
		JLabel lblBase_Rectangulo = new JLabel("Introduce la BASE del RECTANGULO : ");
		lblBase_Rectangulo.setBounds(10, 11, 775, 44);
		Panel_Rectangulo.add(lblBase_Rectangulo);
		lblBase_Rectangulo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtBase_Rectangulo = new JTextField();
		txtBase_Rectangulo.setBounds(10, 55, 328, 32);
		Panel_Rectangulo.add(txtBase_Rectangulo);
		txtBase_Rectangulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtBase_Rectangulo.setColumns(10);
		
		JLabel lblAltura_Rectangulo = new JLabel("Introduce la ALTURA del RECTANGULO : ");
		lblAltura_Rectangulo.setBounds(10, 97, 775, 44);
		Panel_Rectangulo.add(lblAltura_Rectangulo);
		lblAltura_Rectangulo.setFont(new Font("Segoe Script", Font.BOLD, 15));
		
		txtAltura_Rectangulo = new JTextField();
		txtAltura_Rectangulo.setBounds(10, 141, 328, 32);
		Panel_Rectangulo.add(txtAltura_Rectangulo);
		txtAltura_Rectangulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtAltura_Rectangulo.setColumns(10);
		
		JButton btnCalcular_Rectangulo = new JButton("CALCULAR AREA");
		btnCalcular_Rectangulo.setBounds(51, 184, 214, 46);
		Panel_Rectangulo.add(btnCalcular_Rectangulo);
		btnCalcular_Rectangulo.setFont(new Font("Tekton Pro", Font.BOLD | Font.ITALIC, 20));
		
		Panel_Rectangulo.setVisible(false);
		
		DesplegableElegirFigura.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (DesplegableElegirFigura.getSelectedIndex() == 0) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(false);
					
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 1) {
					Panel_Circulo.setVisible(true);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(false);
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 2) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(true);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(false);
					
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 3) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(true);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(false);
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 4) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(true);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(false);
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 5) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(true);
					Panel_Rectangulo.setVisible(false);
				}
				else if (DesplegableElegirFigura.getSelectedIndex() == 6) {
					Panel_Circulo.setVisible(false);
					Panel_Cuadrado.setVisible(false);
					Panel_Triangulo.setVisible(false);
					Panel_Rombo.setVisible(false);
					Panel_Trapecio.setVisible(false);
					Panel_Rectangulo.setVisible(true);
				}
			}
		});
		
		// BOTON CALCULAR
		btnCalcular_Circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double circuloRadio = Double.parseDouble(txtRadio_Circulo.getText());
					area = Math.PI * Math.pow(circuloRadio, 2);
					JOptionPane.showMessageDialog(panel, "El �rea del CIRCULO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		btnCalcular_Cuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double cuadradoLado = Double.parseDouble(txtLado_Cuadrado.getText());
					area = cuadradoLado * cuadradoLado;
					JOptionPane.showMessageDialog(panel, "El �rea del CUADRADO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		btnCalcular_Triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double trianguloBase = Double.parseDouble(txtBase_Triangulo.getText());
					double trianguloAltura = Double.parseDouble(txtAltura_Triangulo.getText());
					area = (trianguloBase * trianguloAltura) / 2;
					JOptionPane.showMessageDialog(panel, "El �rea del TRIANGULO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		btnCalcular_Rombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double romboDMayor = Double.parseDouble(txtDMayor_Rombo.getText());
					double romboDMenor = Double.parseDouble(txtDMenor_Rombo.getText());
					area = (romboDMenor * romboDMayor) / 2;
					JOptionPane.showMessageDialog(panel, "El �rea del ROMBO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		btnCalcular_Trapecio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double trapecioBMayor = Double.parseDouble(txtBMayor_Trapecio.getText());
					double trapecioBMenor = Double.parseDouble(txtBMayor_Trapecio.getText());
					double trapecioAltura = Double.parseDouble(txtAltura_Trapecio.getText());
					area = ((trapecioBMayor + trapecioBMenor) / 2) * trapecioAltura;
					JOptionPane.showMessageDialog(panel, "El �rea del TRAPECIO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		btnCalcular_Rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double rectanguloBase = Double.parseDouble(txtBase_Rectangulo.getText());
					double rectanguloAltura = Double.parseDouble(txtAltura_Rectangulo.getText());
					area = rectanguloBase * rectanguloAltura;
					JOptionPane.showMessageDialog(panel, "El �rea del RECTANGULO es: " + area);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(panel, "Comprueba si lo que has introducido son n�mero o el campo se encuentra vac�o.");
				}
			}
		});
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(Ventana,
						"�Realmente quieres salir?", "Confirmaci�n",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
						if (respuesta == 0)
							Ventana.dispose();
			}
		});
	}
}
