
public class RepasoJava {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// EJERCICIO 2
		String saludo = "hola";
		int numero = 1;
		char letra = 'a';
		double decimal = 0.1;
		boolean hombre = true;
		
		System.out.println("EJ2: CREAR VARIABLES Y MOSTRAR CONTENIDO");
		System.out.println("_________________________________________________");
		System.out.println(saludo + " ," + numero  + " ," + letra  + " ," + decimal  + " ," + hombre);
		System.out.println("\n");
		
		// EJERCICIO 3
		int [] enteros = {1,2,3,4,5,6,7,8,9,10};
		
		System.out.println("EJ3: ARRAY Y MOSTRARLO");
		System.out.println("_________________________________________________");
		for (int i=0; i<10 ; i++) {
			if (enteros[i] % 2 == 0) {
				System.out.println(enteros[i]+ "\t");
			}
		}
		System.out.println("\n");
		
		// EJERCICIO 4
		int [][] matriz = { 
			{1,1,1},
			{1,5,1} 
		};
		int j = 0;

		System.out.println("EJ4: SUMA ARRAY N x N Y MOSTRARLO");
		System.out.println("_________________________________________________");
		for (int x=0; x < matriz.length; x++) {
			for (int y=0; y < matriz[x].length; y++) {
				j = j + matriz[x][y];
			}
			
		}
		System.out.println(j);
		System.out.println("\n");
		
		// EJERCICIO 5
		System.out.println("EJ5: FACTORIAL");
		System.out.println("_________________________________________________");
		System.out.println(factorial(5));
		
	}

	// EJERCICIO 5 - FUNCION
	static int factorial(int num){
        if (num <= 1)
            return 1;
        else
            return num * factorial(num-1);
    }
	
}
