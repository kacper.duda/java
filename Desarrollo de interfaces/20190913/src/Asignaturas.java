
public class Asignaturas {

	String nombre;
	int nhoras;
	String profesor;
	boolean convalidable;
	
	public Asignaturas(String nombre, int nhoras, String profesor, boolean convalidable) {
		super();
		this.nombre = nombre;
		this.nhoras = nhoras;
		this.profesor = profesor;
		this.convalidable = convalidable;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNhoras() {
		return nhoras;
	}

	public void setNhoras(int nhoras) {
		this.nhoras = nhoras;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public boolean isConvalidable() {
		return convalidable;
	}

	public void setConvalidable(boolean convalidable) {
		this.convalidable = convalidable;
	}

	@Override
	public String toString() {
		return "Asignaturas [nombre=" + nombre + ", nhoras=" + nhoras + ", profesor=" + profesor + ", convalidable="
				+ convalidable + "]";
	}
	
}
