
public class Administracion extends Trabajadores {

	char sexo; /* "H" Hombre || "M" Mujer */
	int hextra;
	
	public Administracion(String dni, String nombre, String apellidos, double salario, char sexo, int hextra) {
		super(dni, nombre, apellidos, salario);
		this.sexo = sexo;
		this.hextra = hextra;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public int getHextra() {
		return hextra;
	}

	public void setHextra(int hextra) {
		this.hextra = hextra;
	}

	@Override
	public String toString() {
		return "Profesores [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario
				+ ", sexo=" + sexo + ", hextra=" + hextra + "]";
	}
	
	
}
