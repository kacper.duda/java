
public class Alumnos {
	
	String dni;
	String nombre;
	String apellidos;
	String fechanacimiento;
	char sexo; /* "H" Hombre || "M" Mujer */
	boolean repetidor;
	
	public Alumnos(String dni, String nombre, String apellidos, String fechanacimiento, char sexo, boolean repetidor) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechanacimiento = fechanacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	@Override
	public String toString() {
		return "Alumnos [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", fechanacimiento="
				+ fechanacimiento + ", sexo=" + sexo + ", repetidor=" + repetidor + "]";
	}
	
}
