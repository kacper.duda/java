
public class Directivos extends Trabajadores {

	boolean salesiano;
	char turno; /* "M" - Ma�ana || "T" - Tarde */
	
	public Directivos(String dni, String nombre, String apellidos, double salario, boolean salesiano, char turno) {
		super(dni, nombre, apellidos, salario);
		this.salesiano = salesiano;
		this.turno = turno;
	}

	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}

	public char getTurno() {
		return turno;
	}

	public void setTurno(char turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Directivos [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario
				+ ", salesiano=" + salesiano + ", turno=" + turno + "]";
	}
	
}
