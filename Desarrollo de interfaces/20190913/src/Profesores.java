
public class Profesores extends Trabajadores {

	int nasignaturas; 
	boolean tutor;
	
	public Profesores(String dni, String nombre, String apellidos, double salario, int nasignaturas, boolean tutor) {
		super(dni, nombre, apellidos, salario);
		this.nasignaturas = nasignaturas;
		this.tutor = tutor;
	}

	public int getNasignaturas() {
		return nasignaturas;
	}

	public void setNasignaturas(int nasignaturas) {
		this.nasignaturas = nasignaturas;
	}

	public boolean getTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}

	@Override
	public String toString() {
		return "Profesores [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario
				+ ", nasignaturas=" + nasignaturas + ", tutor=" + tutor + "]";
	}
	
	
}
