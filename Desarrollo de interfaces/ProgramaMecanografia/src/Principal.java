import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ActionEvent;
import java.awt.Cursor;
import java.awt.SystemColor;
import java.awt.TexturePaint;

public class Principal {

	Dificultad dificultad;
	private JTextField textFieldPTotales;
	private JTextField textFieldPMinuto;
	private JTextField textFieldErroresTotales;
	private JTextField textFieldTiempo;
	private JFrame VentanaPrincipal;
	private String NombreUsuario;
	private static int PTotales = 0;
	private int PMinuto = 0;
	private int seg = 0;
	private int min = 0;
	private int errores = 0;
	private int dificultadSelect = 0;
	char textoInsertado[] = null;
	static char textoFacil[];
	static char textoDificil[];
	Timer cronometro;
	Date fecha;
	JButton jbuttonaux = null;
	static String err;

	private boolean startcron = false;
	private static boolean stopcron = false;
	private static boolean finalizado = false;
	
	public JFrame getVentanaPrincipal() {
		return VentanaPrincipal;
	}

	public void setVentanaPrincipal(JFrame VentanaPrincipal) {
		this.VentanaPrincipal = VentanaPrincipal;
	}

	public Principal(Dificultad dificultad, int dificultadSeleccionada, String usuarioNombre) {
		NombreUsuario = usuarioNombre;
		dificultadSelect = dificultadSeleccionada;
		initialize();
		this.dificultad = dificultad;
	}
	
	private void initialize() {
		Dificultad.getVentanaDificultad().dispose();
		
		VentanaPrincipal = new JFrame();
		VentanaPrincipal.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\vacio.png"));
		VentanaPrincipal.getContentPane().setBackground(Color.BLACK);
		VentanaPrincipal.setTitle("Programa MECANOGRAF\u00CDA");
		VentanaPrincipal.setUndecorated(true);
		VentanaPrincipal.setBounds(500,150,1920,1080);
		VentanaPrincipal.setVisible(true);
		VentanaPrincipal.setResizable(false);
		VentanaPrincipal.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		VentanaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		VentanaPrincipal.getContentPane().setLayout(null);
		
		JPanel panel_header = new JPanel();
		panel_header.setBackground(Color.BLACK);
		panel_header.setBounds(10, 11, 1900, 139);
		VentanaPrincipal.getContentPane().add(panel_header);
		panel_header.setLayout(null);
		
		JLabel lblProgramaDeMecanografia = new JLabel("| PROGRAMA DE MECANOGRAF\u00CDA |");
		lblProgramaDeMecanografia.setForeground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setBackground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setHorizontalTextPosition(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setBorder(BorderFactory.createMatteBorder(2,15,2,15,Color.WHITE));
		lblProgramaDeMecanografia.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblProgramaDeMecanografia.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 35));
		lblProgramaDeMecanografia.setBounds(10, 11, 638, 80);
		panel_header.add(lblProgramaDeMecanografia);
		
		JLabel lblBarraHeader = new JLabel("");
		lblBarraHeader.setForeground(new Color(255, 255, 255));
		lblBarraHeader.setBackground(new Color(255, 255, 255));
		lblBarraHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBarraHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblBarraHeader.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		lblBarraHeader.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		lblBarraHeader.setAlignmentX(0.5f);
		lblBarraHeader.setBounds(10, 102, 1880, 29);
		panel_header.add(lblBarraHeader);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSalir.setFocusTraversalKeysEnabled(false);
		btnSalir.setFocusPainted(false);
		btnSalir.setBackground(Color.WHITE);
		btnSalir.setBounds(1729, 60, 161, 56);
		panel_header.add(btnSalir);
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVolver.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnVolver.setFocusTraversalKeysEnabled(false);
		btnVolver.setFocusPainted(false);
		btnVolver.setBackground(Color.WHITE);
		btnVolver.setBounds(1558, 60, 161, 56);
		panel_header.add(btnVolver);
		
		JPanel panel_contenido = new JPanel();
		panel_contenido.setBackground(Color.BLACK);
		panel_contenido.setBounds(20, 161, 1890, 908);
		VentanaPrincipal.getContentPane().add(panel_contenido);
		panel_contenido.setLayout(null);
		
		JButton tecla_1 = new JButton("1");
		tecla_1.setForeground(Color.BLACK);
		tecla_1.setBackground(Color.WHITE);
		tecla_1.setEnabled(false);
		tecla_1.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_1.setBounds(75, 568, 55, 49);
		panel_contenido.add(tecla_1);
		
		JButton tecla_2 = new JButton("2");
		tecla_2.setForeground(Color.BLACK);
		tecla_2.setBackground(Color.WHITE);
		tecla_2.setEnabled(false);
		tecla_2.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_2.setBounds(140, 568, 55, 49);
		panel_contenido.add(tecla_2);
		
		JButton tecla_3 = new JButton("3");
		tecla_3.setForeground(Color.BLACK);
		tecla_3.setBackground(Color.WHITE);
		tecla_3.setEnabled(false);
		tecla_3.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_3.setBounds(205, 568, 55, 49);
		panel_contenido.add(tecla_3);
		
		JButton tecla_4 = new JButton("4");
		tecla_4.setForeground(Color.BLACK);
		tecla_4.setBackground(Color.WHITE);
		tecla_4.setEnabled(false);
		tecla_4.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_4.setBounds(270, 568, 55, 49);
		panel_contenido.add(tecla_4);
		
		JButton tecla_5 = new JButton("5");
		tecla_5.setForeground(Color.BLACK);
		tecla_5.setBackground(Color.WHITE);
		tecla_5.setEnabled(false);
		tecla_5.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_5.setBounds(335, 568, 55, 49);
		panel_contenido.add(tecla_5);
		
		JButton tecla_6 = new JButton("6");
		tecla_6.setForeground(Color.BLACK);
		tecla_6.setBackground(Color.WHITE);
		tecla_6.setEnabled(false);
		tecla_6.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_6.setBounds(400, 568, 55, 49);
		panel_contenido.add(tecla_6);
		
		JButton tecla_7 = new JButton("7");
		tecla_7.setForeground(Color.BLACK);
		tecla_7.setBackground(Color.WHITE);
		tecla_7.setEnabled(false);
		tecla_7.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_7.setBounds(465, 568, 55, 49);
		panel_contenido.add(tecla_7);
		
		JButton tecla_8 = new JButton("8");
		tecla_8.setForeground(Color.BLACK);
		tecla_8.setBackground(Color.WHITE);
		tecla_8.setEnabled(false);
		tecla_8.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_8.setBounds(530, 568, 55, 49);
		panel_contenido.add(tecla_8);
		
		JButton tecla_9 = new JButton("9");
		tecla_9.setForeground(Color.BLACK);
		tecla_9.setBackground(Color.WHITE);
		tecla_9.setEnabled(false);
		tecla_9.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_9.setBounds(595, 568, 55, 49);
		panel_contenido.add(tecla_9);
		
		JButton tecla_10 = new JButton("0");
		tecla_10.setForeground(Color.BLACK);
		tecla_10.setBackground(Color.WHITE);
		tecla_10.setEnabled(false);
		tecla_10.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_10.setBounds(660, 568, 55, 49);
		panel_contenido.add(tecla_10);
		
		JButton tecla_11 = new JButton("Q");
		tecla_11.setForeground(Color.BLACK);
		tecla_11.setBackground(Color.WHITE);
		tecla_11.setEnabled(false);
		tecla_11.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_11.setBounds(104, 628, 55, 49);
		panel_contenido.add(tecla_11);
		
		JButton tecla_12 = new JButton("W");
		tecla_12.setForeground(Color.BLACK);
		tecla_12.setBackground(Color.WHITE);
		tecla_12.setEnabled(false);
		tecla_12.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_12.setBounds(169, 628, 55, 49);
		panel_contenido.add(tecla_12);
		
		JButton tecla_13 = new JButton("E");
		tecla_13.setForeground(Color.BLACK);
		tecla_13.setBackground(Color.WHITE);
		tecla_13.setEnabled(false);
		tecla_13.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_13.setBounds(234, 628, 55, 49);
		panel_contenido.add(tecla_13);
		
		JButton tecla_14 = new JButton("R");
		tecla_14.setForeground(Color.BLACK);
		tecla_14.setBackground(Color.WHITE);
		tecla_14.setEnabled(false);
		tecla_14.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_14.setBounds(299, 628, 55, 49);
		panel_contenido.add(tecla_14);
		
		JButton tecla_15 = new JButton("T");
		tecla_15.setForeground(Color.BLACK);
		tecla_15.setBackground(Color.WHITE);
		tecla_15.setEnabled(false);
		tecla_15.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_15.setBounds(364, 628, 55, 49);
		panel_contenido.add(tecla_15);
		
		JButton tecla_16 = new JButton("Y");
		tecla_16.setForeground(Color.BLACK);
		tecla_16.setBackground(Color.WHITE);
		tecla_16.setEnabled(false);
		tecla_16.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_16.setBounds(429, 628, 55, 49);
		panel_contenido.add(tecla_16);
		
		JButton tecla_17 = new JButton("U");
		tecla_17.setForeground(Color.BLACK);
		tecla_17.setBackground(Color.WHITE);
		tecla_17.setEnabled(false);
		tecla_17.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_17.setBounds(494, 628, 55, 49);
		panel_contenido.add(tecla_17);
		
		JButton tecla_18 = new JButton("I");
		tecla_18.setForeground(Color.BLACK);
		tecla_18.setBackground(Color.WHITE);
		tecla_18.setEnabled(false);
		tecla_18.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_18.setBounds(559, 628, 55, 49);
		panel_contenido.add(tecla_18);
		
		JButton tecla_19 = new JButton("O");
		tecla_19.setForeground(Color.BLACK);
		tecla_19.setBackground(Color.WHITE);
		tecla_19.setEnabled(false);
		tecla_19.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_19.setBounds(624, 628, 55, 49);
		panel_contenido.add(tecla_19);
		
		JButton tecla_20 = new JButton("P");
		tecla_20.setForeground(Color.BLACK);
		tecla_20.setBackground(Color.WHITE);
		tecla_20.setEnabled(false);
		tecla_20.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_20.setBounds(689, 628, 55, 49);
		panel_contenido.add(tecla_20);
		
		JButton tecla_21 = new JButton("A");
		tecla_21.setForeground(Color.BLACK);
		tecla_21.setBackground(Color.WHITE);
		tecla_21.setEnabled(false);
		tecla_21.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_21.setBounds(129, 688, 55, 49);
		panel_contenido.add(tecla_21);
		
		JButton tecla_22 = new JButton("S");
		tecla_22.setForeground(Color.BLACK);
		tecla_22.setBackground(Color.WHITE);
		tecla_22.setEnabled(false);
		tecla_22.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_22.setBounds(194, 688, 55, 49);
		panel_contenido.add(tecla_22);
		
		JButton tecla_23 = new JButton("D");
		tecla_23.setForeground(Color.BLACK);
		tecla_23.setBackground(Color.WHITE);
		tecla_23.setEnabled(false);
		tecla_23.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_23.setBounds(259, 688, 55, 49);
		panel_contenido.add(tecla_23);
		
		JButton tecla_24 = new JButton("F");
		tecla_24.setForeground(Color.BLACK);
		tecla_24.setBackground(Color.WHITE);
		tecla_24.setEnabled(false);
		tecla_24.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_24.setBounds(324, 688, 55, 49);
		panel_contenido.add(tecla_24);
		
		JButton tecla_25 = new JButton("G");
		tecla_25.setForeground(Color.BLACK);
		tecla_25.setBackground(Color.WHITE);
		tecla_25.setEnabled(false);
		tecla_25.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_25.setBounds(387, 688, 55, 49);
		panel_contenido.add(tecla_25);
		
		JButton tecla_26 = new JButton("H");
		tecla_26.setForeground(Color.BLACK);
		tecla_26.setBackground(Color.WHITE);
		tecla_26.setEnabled(false);
		tecla_26.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_26.setBounds(452, 688, 55, 49);
		panel_contenido.add(tecla_26);
		
		JButton tecla_27 = new JButton("J");
		tecla_27.setForeground(Color.BLACK);
		tecla_27.setBackground(Color.WHITE);
		tecla_27.setEnabled(false);
		tecla_27.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_27.setBounds(517, 688, 55, 49);
		panel_contenido.add(tecla_27);
		
		JButton tecla_28 = new JButton("K");
		tecla_28.setForeground(Color.BLACK);
		tecla_28.setBackground(Color.WHITE);
		tecla_28.setEnabled(false);
		tecla_28.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_28.setBounds(582, 688, 55, 49);
		panel_contenido.add(tecla_28);
		
		JButton tecla_29 = new JButton("L");
		tecla_29.setForeground(Color.BLACK);
		tecla_29.setBackground(Color.WHITE);
		tecla_29.setEnabled(false);
		tecla_29.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_29.setBounds(647, 688, 55, 49);
		panel_contenido.add(tecla_29);
		
		JButton tecla_30 = new JButton("\u00D1");
		tecla_30.setForeground(Color.BLACK);
		tecla_30.setBackground(Color.WHITE);
		tecla_30.setEnabled(false);
		tecla_30.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_30.setBounds(712, 688, 55, 49);
		panel_contenido.add(tecla_30);
		
		JButton tecla_31 = new JButton("Z");
		tecla_31.setForeground(Color.BLACK);
		tecla_31.setBackground(Color.WHITE);
		tecla_31.setEnabled(false);
		tecla_31.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_31.setBounds(159, 748, 55, 49);
		panel_contenido.add(tecla_31);
		
		JButton tecla_32 = new JButton("X");
		tecla_32.setForeground(Color.BLACK);
		tecla_32.setBackground(Color.WHITE);
		tecla_32.setEnabled(false);
		tecla_32.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_32.setBounds(224, 748, 55, 49);
		panel_contenido.add(tecla_32);
		
		JButton tecla_33 = new JButton("C");
		tecla_33.setForeground(Color.BLACK);
		tecla_33.setBackground(Color.WHITE);
		tecla_33.setEnabled(false);
		tecla_33.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_33.setBounds(289, 748, 55, 49);
		panel_contenido.add(tecla_33);
		
		JButton tecla_34 = new JButton("V");
		tecla_34.setForeground(Color.BLACK);
		tecla_34.setBackground(Color.WHITE);
		tecla_34.setEnabled(false);
		tecla_34.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_34.setBounds(354, 748, 55, 49);
		panel_contenido.add(tecla_34);
		
		JButton tecla_35 = new JButton("B");
		tecla_35.setForeground(Color.BLACK);
		tecla_35.setBackground(Color.WHITE);
		tecla_35.setEnabled(false);
		tecla_35.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_35.setBounds(419, 748, 55, 49);
		panel_contenido.add(tecla_35);
		
		JButton tecla_36 = new JButton("N");
		tecla_36.setForeground(Color.BLACK);
		tecla_36.setBackground(Color.WHITE);
		tecla_36.setEnabled(false);
		tecla_36.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_36.setBounds(484, 748, 55, 49);
		panel_contenido.add(tecla_36);
		
		JButton tecla_37 = new JButton("M");
		tecla_37.setForeground(Color.BLACK);
		tecla_37.setBackground(Color.WHITE);
		tecla_37.setEnabled(false);
		tecla_37.setFont(new Font("Dialog", Font.BOLD, 16));
		tecla_37.setBounds(549, 748, 55, 49);
		panel_contenido.add(tecla_37);
		
		JButton vacio_1 = new JButton("");
		vacio_1.setForeground(Color.BLACK);
		vacio_1.setBackground(Color.WHITE);
		vacio_1.setEnabled(false);
		vacio_1.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_1.setBounds(614, 748, 55, 49);
		panel_contenido.add(vacio_1);
		
		JButton vacio_2 = new JButton("");
		vacio_2.setForeground(Color.BLACK);
		vacio_2.setBackground(Color.WHITE);
		vacio_2.setEnabled(false);
		vacio_2.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_2.setBounds(679, 748, 55, 49);
		panel_contenido.add(vacio_2);
		
		JButton vacio_espacio = new JButton("");
		vacio_espacio.setForeground(Color.BLACK);
		vacio_espacio.setBackground(Color.WHITE);
		vacio_espacio.setEnabled(false);
		vacio_espacio.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_espacio.setBounds(270, 808, 435, 49);
		panel_contenido.add(vacio_espacio);
		
		JButton vacio_4 = new JButton("");
		vacio_4.setForeground(Color.BLACK);
		vacio_4.setBackground(Color.WHITE);
		vacio_4.setEnabled(false);
		vacio_4.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_4.setBounds(857, 568, 109, 49);
		panel_contenido.add(vacio_4);
		
		JButton vacio_5 = new JButton("");
		vacio_5.setForeground(Color.BLACK);
		vacio_5.setBackground(Color.WHITE);
		vacio_5.setEnabled(false);
		vacio_5.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_5.setBounds(809, 748, 157, 49);
		panel_contenido.add(vacio_5);
		
		JButton vacio_6 = new JButton("");
		vacio_6.setForeground(Color.BLACK);
		vacio_6.setBackground(Color.WHITE);
		vacio_6.setEnabled(false);
		vacio_6.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_6.setBounds(725, 568, 55, 49);
		panel_contenido.add(vacio_6);
		
		JButton vacio_7 = new JButton("");
		vacio_7.setForeground(Color.BLACK);
		vacio_7.setBackground(Color.WHITE);
		vacio_7.setEnabled(false);
		vacio_7.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_7.setBounds(790, 568, 55, 49);
		panel_contenido.add(vacio_7);
		
		JButton vacio_8 = new JButton("");
		vacio_8.setForeground(Color.BLACK);
		vacio_8.setBackground(Color.WHITE);
		vacio_8.setEnabled(false);
		vacio_8.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_8.setBounds(744, 748, 55, 49);
		panel_contenido.add(vacio_8);
		
		JButton vacio_9 = new JButton("");
		vacio_9.setForeground(Color.BLACK);
		vacio_9.setBackground(Color.WHITE);
		vacio_9.setEnabled(false);
		vacio_9.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_9.setBounds(754, 628, 55, 49);
		panel_contenido.add(vacio_9);
		
		JButton vacio_10 = new JButton("");
		vacio_10.setForeground(Color.BLACK);
		vacio_10.setBackground(Color.WHITE);
		vacio_10.setEnabled(false);
		vacio_10.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_10.setBounds(777, 688, 55, 49);
		panel_contenido.add(vacio_10);
		
		JButton vacio_11 = new JButton("");
		vacio_11.setForeground(Color.BLACK);
		vacio_11.setBackground(Color.WHITE);
		vacio_11.setEnabled(false);
		vacio_11.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_11.setBounds(819, 628, 55, 49);
		panel_contenido.add(vacio_11);
		
		JButton vacio_12 = new JButton("");
		vacio_12.setForeground(Color.BLACK);
		vacio_12.setBackground(Color.WHITE);
		vacio_12.setEnabled(false);
		vacio_12.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_12.setBounds(10, 568, 55, 49);
		panel_contenido.add(vacio_12);
		
		JButton vacio_13 = new JButton("");
		vacio_13.setForeground(Color.BLACK);
		vacio_13.setBackground(Color.WHITE);
		vacio_13.setEnabled(false);
		vacio_13.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_13.setBounds(10, 628, 84, 49);
		panel_contenido.add(vacio_13);
		
		JButton vacio_14 = new JButton("");
		vacio_14.setForeground(Color.BLACK);
		vacio_14.setBackground(Color.WHITE);
		vacio_14.setEnabled(false);
		vacio_14.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_14.setBounds(10, 688, 109, 49);
		panel_contenido.add(vacio_14);
		
		JButton vacio_15 = new JButton("");
		vacio_15.setForeground(Color.BLACK);
		vacio_15.setBackground(Color.WHITE);
		vacio_15.setEnabled(false);
		vacio_15.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_15.setBounds(10, 748, 139, 49);
		panel_contenido.add(vacio_15);
		
		JButton vacio_16 = new JButton("");
		vacio_16.setForeground(Color.BLACK);
		vacio_16.setBackground(Color.WHITE);
		vacio_16.setEnabled(false);
		vacio_16.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_16.setBounds(10, 808, 84, 49);
		panel_contenido.add(vacio_16);
		
		JButton vacio_17 = new JButton("");
		vacio_17.setForeground(Color.BLACK);
		vacio_17.setBackground(Color.WHITE);
		vacio_17.setEnabled(false);
		vacio_17.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_17.setBounds(169, 808, 91, 49);
		panel_contenido.add(vacio_17);
		
		JButton vacio_18 = new JButton("");
		vacio_18.setForeground(Color.BLACK);
		vacio_18.setBackground(Color.WHITE);
		vacio_18.setEnabled(false);
		vacio_18.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_18.setBounds(712, 808, 107, 49);
		panel_contenido.add(vacio_18);
		
		JButton vacio_19 = new JButton("");
		vacio_19.setForeground(Color.BLACK);
		vacio_19.setBackground(Color.WHITE);
		vacio_19.setEnabled(false);
		vacio_19.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_19.setBounds(894, 808, 72, 49);
		panel_contenido.add(vacio_19);
		
		JButton vacio_20 = new JButton("");
		vacio_20.setForeground(Color.BLACK);
		vacio_20.setBackground(Color.WHITE);
		vacio_20.setEnabled(false);
		vacio_20.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_20.setBounds(911, 676, 55, 61);
		panel_contenido.add(vacio_20);
		
		JButton vacio_21 = new JButton("");
		vacio_21.setForeground(Color.BLACK);
		vacio_21.setBackground(Color.WHITE);
		vacio_21.setEnabled(false);
		vacio_21.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_21.setBounds(884, 628, 82, 49);
		panel_contenido.add(vacio_21);
		
		JButton vacio_22 = new JButton("");
		vacio_22.setForeground(Color.BLACK);
		vacio_22.setBackground(Color.WHITE);
		vacio_22.setEnabled(false);
		vacio_22.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_22.setBounds(842, 688, 55, 49);
		panel_contenido.add(vacio_22);
		
		JButton vacio_23 = new JButton("");
		vacio_23.setForeground(Color.BLACK);
		vacio_23.setBackground(Color.WHITE);
		vacio_23.setEnabled(false);
		vacio_23.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_23.setBounds(104, 808, 55, 49);
		panel_contenido.add(vacio_23);
		
		JButton vacio_24 = new JButton("");
		vacio_24.setForeground(Color.BLACK);
		vacio_24.setBackground(Color.WHITE);
		vacio_24.setEnabled(false);
		vacio_24.setFont(new Font("Dialog", Font.BOLD, 16));
		vacio_24.setBounds(829, 808, 55, 49);
		panel_contenido.add(vacio_24);
		
		JLabel labelSeparador1 = new JLabel("");
		labelSeparador1.setEnabled(false);
		labelSeparador1.setHorizontalTextPosition(SwingConstants.CENTER);
		labelSeparador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelSeparador1.setForeground(Color.WHITE);
		labelSeparador1.setFont(new Font("Dialog", Font.BOLD, 18));
		labelSeparador1.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		labelSeparador1.setBackground(Color.WHITE);
		labelSeparador1.setAlignmentX(0.5f);
		labelSeparador1.setBounds(10, 501, 960, 29);
		panel_contenido.add(labelSeparador1);
		
		JLabel labelSeparador2 = new JLabel("");
		labelSeparador2.setEnabled(false);
		labelSeparador2.setHorizontalTextPosition(SwingConstants.CENTER);
		labelSeparador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelSeparador2.setForeground(Color.WHITE);
		labelSeparador2.setFont(new Font("Dialog", Font.BOLD, 18));
		labelSeparador2.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		labelSeparador2.setBackground(Color.WHITE);
		labelSeparador2.setAlignmentX(0.5f);
		labelSeparador2.setBounds(10, 868, 960, 29);
		panel_contenido.add(labelSeparador2);
		
		JEditorPane textPanelTexto = new JEditorPane();
		textPanelTexto.setEditable(false);
		textPanelTexto.setBackground(UIManager.getColor("Button.light"));
		textPanelTexto.setFont(new Font("Tahoma", Font.BOLD, 25));
		textPanelTexto.setBounds(10, 11, 960, 235);
		panel_contenido.add(textPanelTexto);
		
		JLabel labelMarcoEstadisticas = new JLabel("");
		labelMarcoEstadisticas.setHorizontalTextPosition(SwingConstants.CENTER);
		labelMarcoEstadisticas.setHorizontalAlignment(SwingConstants.CENTER);
		labelMarcoEstadisticas.setForeground(Color.WHITE);
		labelMarcoEstadisticas.setFont(new Font("Dialog", Font.BOLD, 18));
		labelMarcoEstadisticas.setEnabled(false);
		labelMarcoEstadisticas.setBorder(new MatteBorder(10, 10, 10, 10, (Color) new Color(255, 255, 255)));
		labelMarcoEstadisticas.setBackground(Color.WHITE);
		labelMarcoEstadisticas.setAlignmentX(0.5f);
		labelMarcoEstadisticas.setBounds(999, 102, 730, 504);
		panel_contenido.add(labelMarcoEstadisticas);
		
		JPanel panelEstadisticas = new JPanel();
		panelEstadisticas.setBackground(Color.BLACK);
		panelEstadisticas.setBounds(999, 102, 730, 504);
		panel_contenido.add(panelEstadisticas);
		panelEstadisticas.setLayout(null);
		
		JLabel lblPTotales = new JLabel("Pulsaciones Totales");
		lblPTotales.setBounds(67, 70, 214, 58);
		panelEstadisticas.add(lblPTotales);
		lblPTotales.setBackground(Color.GRAY);
		lblPTotales.setHorizontalAlignment(SwingConstants.CENTER);
		lblPTotales.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPTotales.setForeground(Color.WHITE);
		
		JLabel lblPMinuto = new JLabel("Pulsaciones/Minuto");
		lblPMinuto.setHorizontalAlignment(SwingConstants.CENTER);
		lblPMinuto.setForeground(Color.WHITE);
		lblPMinuto.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPMinuto.setBounds(436, 70, 214, 58);
		panelEstadisticas.add(lblPMinuto);
		
		JLabel lblErroresTotales = new JLabel("Total Errores");
		lblErroresTotales.setHorizontalAlignment(SwingConstants.CENTER);
		lblErroresTotales.setForeground(Color.WHITE);
		lblErroresTotales.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblErroresTotales.setBackground(Color.GRAY);
		lblErroresTotales.setBounds(67, 253, 214, 58);
		panelEstadisticas.add(lblErroresTotales);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTiempo.setForeground(Color.WHITE);
		lblTiempo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTiempo.setBounds(436, 253, 214, 58);
		panelEstadisticas.add(lblTiempo);
		
		textFieldPTotales = new JTextField();
		textFieldPTotales.setEditable(false);
		textFieldPTotales.setText("0");
		textFieldPTotales.setFont(new Font("Tahoma", Font.BOLD, 30));
		textFieldPTotales.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldPTotales.setBounds(67, 135, 214, 58);
		textFieldPTotales.setBorder(BorderFactory.createMatteBorder(0,15,0,0,Color.DARK_GRAY));
		panelEstadisticas.add(textFieldPTotales);
		textFieldPTotales.setColumns(10);
		
		textFieldPMinuto = new JTextField();
		textFieldPMinuto.setEditable(false);
		textFieldPMinuto.setText("0");
		textFieldPMinuto.setFont(new Font("Tahoma", Font.BOLD, 30));
		textFieldPMinuto.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldPMinuto.setColumns(10);
		textFieldPMinuto.setBounds(436, 135, 214, 58);
		textFieldPMinuto.setBorder(BorderFactory.createMatteBorder(0,15,0,0,Color.DARK_GRAY));
		panelEstadisticas.add(textFieldPMinuto);
		
		textFieldErroresTotales = new JTextField();
		textFieldErroresTotales.setEditable(false);
		textFieldErroresTotales.setText("0");
		textFieldErroresTotales.setFont(new Font("Tahoma", Font.BOLD, 30));
		textFieldErroresTotales.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldErroresTotales.setColumns(10);
		textFieldErroresTotales.setBounds(67, 322, 214, 58);
		textFieldErroresTotales.setBorder(BorderFactory.createMatteBorder(0,15,0,0,Color.DARK_GRAY));
		panelEstadisticas.add(textFieldErroresTotales);
		
		textFieldTiempo = new JTextField();
		textFieldTiempo.setEditable(false);
		textFieldTiempo.setText("00:00");
		textFieldTiempo.setFont(new Font("Tahoma", Font.BOLD, 30));
		textFieldTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldTiempo.setColumns(10);
		textFieldTiempo.setBounds(436, 322, 214, 58);
		textFieldTiempo.setBorder(BorderFactory.createMatteBorder(0,15,0,0,Color.DARK_GRAY));
		panelEstadisticas.add(textFieldTiempo);
		
		JEditorPane textPanelInsertado = new JEditorPane();
		textPanelInsertado.setFont(new Font("Tahoma", Font.BOLD, 25));
		textPanelInsertado.setBackground(Color.WHITE);
		textPanelInsertado.setBounds(10, 257, 960, 235);
		panel_contenido.add(textPanelInsertado);
		textPanelInsertado.requestFocus();
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaPrincipal, "�Realmente quieres volver? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR SESI�N", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					Dificultad.getVentanaDificultad().setVisible(true);
					VentanaPrincipal.dispose();
					PTotales = 0;
					PMinuto = 0;
				}
			}
		});
		
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaPrincipal, "�Realmente quieres salir? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR EL PROGRAMA", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0)
					VentanaPrincipal.dispatchEvent(new WindowEvent(VentanaPrincipal, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		LeerDificultad(dificultadSelect, textPanelTexto);

		textPanelInsertado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				if (!startcron) {
					CronometroTexto();
					startcron = true;
				}
				
				if (arg0.getKeyCode() == KeyEvent.VK_1)
					ResaltarTecla('1', tecla_1, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_2)
					ResaltarTecla('2', tecla_2, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_3)
					ResaltarTecla('3', tecla_3, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_4)
					ResaltarTecla('4', tecla_4, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_5)
					ResaltarTecla('5', tecla_5, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_6)
					ResaltarTecla('6', tecla_6, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_7)
					ResaltarTecla('7', tecla_7, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_8)
					ResaltarTecla('8', tecla_8, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_9)
					ResaltarTecla('9', tecla_9, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_0)
					ResaltarTecla('0', tecla_10, textPanelInsertado);

				if (arg0.getKeyCode() == KeyEvent.VK_SPACE)
					ResaltarTecla(' ', vacio_espacio, textPanelInsertado);
				
				if (arg0.getKeyCode() == KeyEvent.VK_BACK_SPACE)
					arg0.consume();
				
				if (arg0.getKeyCode() == KeyEvent.VK_Q) {
					if (PTotales == 0)
						ResaltarTecla('Q', tecla_11, textPanelInsertado);
					else
						ResaltarTecla('q', tecla_11, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_W) {
					if (PTotales == 0)
						ResaltarTecla('W', tecla_12, textPanelInsertado);
					else
						ResaltarTecla('w', tecla_12, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_E) {
					if (PTotales == 0)
						ResaltarTecla('E', tecla_13, textPanelInsertado);
					else
						ResaltarTecla('e', tecla_13, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_R) {
					if (PTotales == 0)
						ResaltarTecla('R', tecla_14, textPanelInsertado);
					else
						ResaltarTecla('r', tecla_14, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_T) {
					if (PTotales == 0)
						ResaltarTecla('T', tecla_15, textPanelInsertado);
					else
						ResaltarTecla('t', tecla_15, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_Y) {
					if (PTotales == 0)
						ResaltarTecla('Y', tecla_16, textPanelInsertado);
					else
						ResaltarTecla('y', tecla_16, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_U) {
					if (PTotales == 0)
						ResaltarTecla('U', tecla_17, textPanelInsertado);
					else
						ResaltarTecla('u', tecla_17, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_I) {
					if (PTotales == 0)
						ResaltarTecla('I', tecla_18, textPanelInsertado);
					else
						ResaltarTecla('i', tecla_18, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_O) {
					if (PTotales == 0)
						ResaltarTecla('O', tecla_19, textPanelInsertado);
					else
						ResaltarTecla('o', tecla_19, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_P) {
					if (PTotales == 0)
						ResaltarTecla('P', tecla_20, textPanelInsertado);
					else
						ResaltarTecla('p', tecla_20, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_A) {
					if (PTotales == 0)
						ResaltarTecla('A', tecla_21, textPanelInsertado);
					else
						ResaltarTecla('a', tecla_21, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_S) {
					if (PTotales == 0)
						ResaltarTecla('S', tecla_22, textPanelInsertado);
					else
						ResaltarTecla('s', tecla_22, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_D) {
					if (PTotales == 0)
						ResaltarTecla('D', tecla_23, textPanelInsertado);
					else
						ResaltarTecla('d', tecla_23, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_F) {
					if (PTotales == 0)
						ResaltarTecla('F', tecla_24, textPanelInsertado);
					else
						ResaltarTecla('f', tecla_24, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_G) {
					if (PTotales == 0)
						ResaltarTecla('G', tecla_25, textPanelInsertado);
					else
						ResaltarTecla('g', tecla_25, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_H) {
					if (PTotales == 0)
						ResaltarTecla('H', tecla_26, textPanelInsertado);
					else
						ResaltarTecla('h', tecla_26, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_J) {
					if (PTotales == 0)
						ResaltarTecla('J', tecla_27, textPanelInsertado);
					else
						ResaltarTecla('j', tecla_27, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_K) {
					if (PTotales == 0)
						ResaltarTecla('K', tecla_28, textPanelInsertado);
					else
						ResaltarTecla('k', tecla_28, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_L) {
					if (PTotales == 0)
						ResaltarTecla('L', tecla_29, textPanelInsertado);
					else
						ResaltarTecla('l', tecla_29, textPanelInsertado);
				}
				
				if (arg0.getKeyChar() == '�') {
					if (PTotales == 0)
						ResaltarTecla('�', tecla_30, textPanelInsertado);
					else
						ResaltarTecla('�', tecla_30, textPanelInsertado);
				}
				
				if (arg0.getKeyChar() == '�') {
					if (PTotales == 0)
						ResaltarTecla('�', tecla_30, textPanelInsertado);
					else
						ResaltarTecla('�', tecla_30, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_Z) {
					if (PTotales == 0)
						ResaltarTecla('Z', tecla_31, textPanelInsertado);
					else
						ResaltarTecla('z', tecla_31, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_X) {
					if (PTotales == 0)
						ResaltarTecla('X', tecla_32, textPanelInsertado);
					else
						ResaltarTecla('x', tecla_32, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_C) {
					if (PTotales == 0)
						ResaltarTecla('C', tecla_33, textPanelInsertado);
					else
						ResaltarTecla('c', tecla_33, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_V) {
					if (PTotales == 0)
						ResaltarTecla('V', tecla_34, textPanelInsertado);
					else
						ResaltarTecla('v', tecla_34, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_B) {
					if (PTotales == 0)
						ResaltarTecla('B', tecla_35, textPanelInsertado);
					else
						ResaltarTecla('b', tecla_35, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_N) {
					if (PTotales == 0)
						ResaltarTecla('N', tecla_36, textPanelInsertado);
					else
						ResaltarTecla('n', tecla_36, textPanelInsertado);
				}

				if (arg0.getKeyCode() == KeyEvent.VK_M) {
					if (PTotales == 0)
						ResaltarTecla('M', tecla_37, textPanelInsertado);
					else
						ResaltarTecla('m', tecla_37, textPanelInsertado);
				}
			};
		});
	}
	
	public void CronometroTexto() {
		cronometro = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seg++;						
				if (stopcron) {
					cronometro.stop();
				} 
				
				if (seg % 60  == 0) {
					min += 1;
				} 
				
				if (seg < 10) {
					textFieldTiempo.setText(String.format("%02d", min) + ":" + String.format("%02d", seg % 60));
				} else {
					textFieldTiempo.setText(String.format("%02d", min) + ":" + String.format("%02d", seg % 60));
				}

				if (min == 5) {
					cronometro.stop();	
				}
				PMinuto = ((PTotales * 60) / seg);
				textFieldPMinuto.setText(Integer.toString(PMinuto));
			}
		});
		cronometro.start();
	}

	public static void LeerDificultad(int dificultadSeleccionada, JEditorPane textPanelTexto) {
		String cadena = "";
		String texto = "";
		String leccion = "";
		String textoLeccion = "";
		String textoFinalLeccion = "";
		int empezar = 0;
		int terminar = 0;
		
		try {
			File inputFichero = new File("files\\dificultad\\leccion.txt");
			BufferedReader br = 
					new BufferedReader(new InputStreamReader(new FileInputStream(inputFichero)));
			
			while((cadena = br.readLine())!=null) {
				texto += cadena;
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dificultadSeleccionada == 1) {
			textoLeccion = "L2";
			leccion = "L1";
		} else if (dificultadSeleccionada == 2) {
			textoLeccion = "FINAL";
			leccion = "L2";
		}
		
		empezar = texto.indexOf(leccion);
		terminar = texto.indexOf(textoLeccion, empezar + 2);
		
		textoFinalLeccion = texto.substring(empezar + 2, terminar);
		
		if (dificultadSeleccionada == 1) {
			textoFacil = textoFinalLeccion.toCharArray();
			for (int i=0; i<textoFacil.length; i++) {
				textPanelTexto.setText(textPanelTexto.getText() + Character.toString(textoFacil[i]));
			}
		} else if (dificultadSeleccionada == 2) {
			textoDificil = textoFinalLeccion.toCharArray();
			for (int i=0; i<textoDificil.length; i++) {
				textPanelTexto.setText(textPanelTexto.getText() + Character.toString(textoDificil[i]));
			}
		}
	}

	public static void EscribirEstadisticas(String usuario, String ptotales, String pminuto, String tiempo) {
		Date Fecha = new Date();
		String strFormatoFecha = "dd-MMM-aaaa  hh:mm";
		SimpleDateFormat FechaF = new SimpleDateFormat(strFormatoFecha);
		
		try {
			File fichero = new File("files\\estadisticas\\" + usuario + ".txt");
			FileWriter fw = new FileWriter(fichero, true);
			fw.write("************************************************************\n");
			fw.write("Usuario: " + usuario + "\n");
			fw.write("Fecha: " + FechaF.format(Fecha) + "\n");
			fw.write("Pulsaciones totales: " + ptotales + "\n");
			fw.write("Pulsaciones por minuto: " + pminuto + "\n");
			fw.write("Tiempo: " + tiempo + "\n");
			fw.write("Errores: " + err + "\n");
			fw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void ResaltarTecla(char letrapulsada, JButton jbutton,JEditorPane textPanelInsertado) {
		if (dificultadSelect == 1) {
			if (textoFacil.length == PTotales) {
				stopcron = true;
				JOptionPane.showMessageDialog(null, "�Felicidades! Has terminado tu leccion.\n�Se ha guardado las estad�sticas!\n\nPara intentarlo de nuevo vuelve al menu principal.");
				textPanelInsertado.setEnabled(false);
				finalizado = true;
				stopcron = false;
			}
			if (finalizado) {
				EscribirEstadisticas(NombreUsuario, textFieldPTotales.getText(), textFieldPMinuto.getText(), textFieldTiempo.getText());
				PTotales = 0;
				PMinuto = 0;
				finalizado = false;
			}
			if (!startcron) {
				CronometroTexto();
				startcron = true;
			}
			if (PTotales != 0)
				jbuttonaux.setBackground(SystemColor.control);

			if (textoFacil[PTotales] == letrapulsada) {
				jbutton.setBackground(Color.GREEN);
			} else {
				jbutton.setBackground(Color.RED);
				errores++;
				err = Integer.toString(errores);
				textFieldErroresTotales.setText(err);
			}
			jbuttonaux = jbutton;
			PTotales++;
		}
		
		if (dificultadSelect == 2) {
			if (textoDificil.length == PTotales) {
				stopcron = true;
				JOptionPane.showMessageDialog(null, "�Felicidades! Has terminado tu leccion.\n�Se ha guardado las estad�sticas!\n\nPara intentarlo de nuevo vuelve al menu principal.");
				textPanelInsertado.setEnabled(false);
				finalizado = true;
				stopcron = false;
			}
			if (finalizado) {
				EscribirEstadisticas(NombreUsuario, textFieldPTotales.getText(), textFieldPMinuto.getText(), textFieldTiempo.getText());
				PTotales = 0;
				PMinuto = 0;
				finalizado = false;
			}
			if (!startcron) {
				CronometroTexto();
				startcron = true;
			}
			if (PTotales != 0)
				jbuttonaux.setBackground(SystemColor.white);

			if (textoDificil[PTotales] == letrapulsada) {
				jbutton.setBackground(Color.GREEN);
			} else {
				jbutton.setBackground(Color.RED);
				errores++;
				String err = Integer.toString(errores);
				textFieldErroresTotales.setText(err);
			}

			jbuttonaux = jbutton;
			PTotales++;
		}
		
	}

}