import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.awt.Cursor;

public class Estadisticas {

	private JFrame VentanaEstadisticas;
	private String NombreUsuario;

	public Estadisticas(Dificultad dificultad, String usuarioNombre) {
		NombreUsuario = usuarioNombre;
		initialize();
	}

	private void initialize() {
		VentanaEstadisticas = new JFrame();
		VentanaEstadisticas.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\vacio.png"));
		VentanaEstadisticas.getContentPane().setBackground(Color.BLACK);
		VentanaEstadisticas.setTitle("Programa MECANOGRAF\u00CDA");
		VentanaEstadisticas.setBounds(500,150,1000,700);
		VentanaEstadisticas.setVisible(true);
		VentanaEstadisticas.setResizable(false); 
		VentanaEstadisticas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		VentanaEstadisticas.getContentPane().setLayout(null);
		
		
		JPanel panel_header = new JPanel();
		panel_header.setBackground(Color.BLACK);
		panel_header.setBounds(10, 11, 974, 139);
		VentanaEstadisticas.getContentPane().add(panel_header);
		panel_header.setLayout(null);
		
		JLabel lblProgramaDeMecanografia = new JLabel("| PROGRAMA DE MECANOGRAF\u00CDA |");
		lblProgramaDeMecanografia.setForeground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setBackground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setHorizontalTextPosition(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setBorder(BorderFactory.createMatteBorder(2,15,2,15,Color.WHITE));
		lblProgramaDeMecanografia.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblProgramaDeMecanografia.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 35));
		lblProgramaDeMecanografia.setBounds(10, 11, 638, 80);
		panel_header.add(lblProgramaDeMecanografia);
		
		JLabel lblBarraHeader = new JLabel("");
		lblBarraHeader.setForeground(new Color(255, 255, 255));
		lblBarraHeader.setBackground(new Color(255, 255, 255));
		lblBarraHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBarraHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblBarraHeader.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		lblBarraHeader.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		lblBarraHeader.setAlignmentX(0.5f);
		lblBarraHeader.setBounds(10, 102, 960, 29);
		panel_header.add(lblBarraHeader);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVolver.setFocusPainted(false);
		btnVolver.setFocusTraversalKeysEnabled(false);
		btnVolver.setBackground(Color.WHITE);
		btnVolver.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnVolver.setBounds(803, 11, 161, 38);
		panel_header.add(btnVolver);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSalir.setFocusPainted(false);
		btnSalir.setFocusTraversalKeysEnabled(false);
		btnSalir.setBackground(Color.WHITE);
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSalir.setBounds(803, 60, 161, 56);
		panel_header.add(btnSalir);
		
		JPanel panel_estadisticas = new JPanel();
		panel_estadisticas.setBackground(Color.BLACK);
		panel_estadisticas.setBounds(10, 161, 974, 499);
		VentanaEstadisticas.getContentPane().add(panel_estadisticas);
		panel_estadisticas.setLayout(null);
		
		JPanel panel_texto = new JPanel();
		panel_texto.setBounds(0, 103, 974, 385);
		panel_estadisticas.add(panel_texto);
		panel_texto.setBackground(Color.BLACK);
		panel_texto.setLayout(null);
		
		JTextArea textAreaEstadisticas = new JTextArea(10,10);
		textAreaEstadisticas.setFont(new Font("Monospaced", Font.BOLD, 18));
		textAreaEstadisticas.setBounds(1, 1, 972, 184);
		panel_texto.add(textAreaEstadisticas);
		textAreaEstadisticas.setLineWrap(true);
		textAreaEstadisticas.setEditable(false);
		textAreaEstadisticas.setCaretPosition(0);
		panel_texto.add("Center", textAreaEstadisticas);
		
		JScrollPane scrollEstadisticas = new JScrollPane(textAreaEstadisticas);
		scrollEstadisticas.setBounds(0, 0, 974, 385);
		panel_texto.add(scrollEstadisticas);
		
		JLabel lblEstadisticas = new JLabel("ESTAD\u00CDSTICAS");
		lblEstadisticas.setBounds(0, 0, 954, 104);
		panel_estadisticas.add(lblEstadisticas);
		lblEstadisticas.setHorizontalAlignment(SwingConstants.LEFT);
		lblEstadisticas.setForeground(Color.WHITE);
		lblEstadisticas.setFont(new Font("Dialog", Font.BOLD, 30));
		
		JLabel lblError = new JLabel("NOTA");
		lblError.setHorizontalAlignment(SwingConstants.LEFT);
		lblError.setForeground(Color.WHITE);
		lblError.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 18));
		lblError.setBounds(0, 70, 973, 104);
		panel_estadisticas.add(lblError);
		
		LeerEstadisticas(NombreUsuario, textAreaEstadisticas, panel_texto, lblError);
		
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaEstadisticas, "�Realmente quieres salir? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR EL PROGRAMA", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0)
					VentanaEstadisticas.dispatchEvent(new WindowEvent(VentanaEstadisticas, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Dificultad.getVentanaDificultad().setVisible(true);
				VentanaEstadisticas.dispose();
			}
		});
	}
	
	public static void LeerEstadisticas(String NombreUsuario, JTextArea textAreaEstadisticas, JPanel panel_texto, JLabel lblNota) {
		try {
			File fichero = new File("files\\estadisticas\\" + NombreUsuario + ".txt");
			FileReader fr = new FileReader(fichero);
			int letra;
			String cadena;
			cadena = "";
			letra = fr.read();
			while (letra != -1) {
				cadena += ((char) letra);
				letra = fr.read();
			}
			fr.close();
			textAreaEstadisticas.setText(cadena);
				
		} catch (FileNotFoundException e) {
			panel_texto.setVisible(false);
			lblNota.setText("Usted no ha realizado ninguna lecci�n, cuando realice una lecci�n su resultado se mostrar� en este apartado.");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
