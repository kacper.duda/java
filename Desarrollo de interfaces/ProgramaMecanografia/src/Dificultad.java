import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Cursor;

public class Dificultad {

	private static JFrame VentanaDificultad;
	Inicio inicio;
	private String NombreUsuario;
	
	public static JFrame getVentanaDificultad() {
		return VentanaDificultad;
	}

	public void setVentanaDificultad(JFrame VentanaDificultad) {
		Dificultad.VentanaDificultad = VentanaDificultad;
	}
	
	public Dificultad(Inicio inicio, String usuarioNombre) {
		NombreUsuario = usuarioNombre;
		initialize();
		this.inicio = inicio;
	}

	private void initialize() {
		
		VentanaDificultad = new JFrame();
		VentanaDificultad.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\vacio.png"));
		VentanaDificultad.getContentPane().setBackground(Color.BLACK);
		VentanaDificultad.setTitle("Programa MECANOGRAF\u00CDA");
		VentanaDificultad.setBounds(500,150,1000,700);
		VentanaDificultad.setVisible(true);
		VentanaDificultad.setResizable(false); 
		VentanaDificultad.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		VentanaDificultad.getContentPane().setLayout(null);
		
		JPanel panel_header = new JPanel();
		panel_header.setBackground(Color.BLACK);
		panel_header.setBounds(10, 11, 974, 139);
		VentanaDificultad.getContentPane().add(panel_header);
		panel_header.setLayout(null);
		
		JLabel lblProgramaDeMecanografia = new JLabel("| PROGRAMA DE MECANOGRAF\u00CDA |");
		lblProgramaDeMecanografia.setForeground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setBackground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setHorizontalTextPosition(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setBorder(BorderFactory.createMatteBorder(2,15,2,15,Color.WHITE));
		lblProgramaDeMecanografia.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblProgramaDeMecanografia.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 35));
		lblProgramaDeMecanografia.setBounds(10, 11, 638, 80);
		panel_header.add(lblProgramaDeMecanografia);
		
		JLabel lblBarraHeader = new JLabel("");
		lblBarraHeader.setForeground(new Color(255, 255, 255));
		lblBarraHeader.setBackground(new Color(255, 255, 255));
		lblBarraHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBarraHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblBarraHeader.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		lblBarraHeader.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		lblBarraHeader.setAlignmentX(0.5f);
		lblBarraHeader.setBounds(10, 102, 960, 29);
		panel_header.add(lblBarraHeader);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVolver.setFocusPainted(false);
		btnVolver.setFocusTraversalKeysEnabled(false);
		btnVolver.setBackground(Color.WHITE);
		btnVolver.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnVolver.setBounds(803, 11, 161, 38);
		panel_header.add(btnVolver);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSalir.setFocusPainted(false);
		btnSalir.setFocusTraversalKeysEnabled(false);
		btnSalir.setBackground(Color.WHITE);
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSalir.setBounds(803, 60, 161, 56);
		panel_header.add(btnSalir);
		
		JPanel panel_dificultad = new JPanel();
		panel_dificultad.setBackground(Color.BLACK);
		panel_dificultad.setBounds(10, 161, 974, 499);
		VentanaDificultad.getContentPane().add(panel_dificultad);
		panel_dificultad.setLayout(null);
		
		JLabel lblDificultad = new JLabel("SELECCIONA UNA DIFICULTAD :");
		lblDificultad.setHorizontalAlignment(SwingConstants.CENTER);
		lblDificultad.setForeground(Color.WHITE);
		lblDificultad.setFont(new Font("Tempus Sans ITC", Font.BOLD, 50));
		lblDificultad.setBounds(10, 124, 954, 163);
		panel_dificultad.add(lblDificultad);
		
		JButton btnDificultad1 = new JButton("DIFICULTAD NORMAL");
		btnDificultad1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnDificultad1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		btnDificultad1.setBounds(118, 300, 358, 163);
		panel_dificultad.add(btnDificultad1);
		
		JButton btnDificultad2 = new JButton("DIFICULTAD DIF\u00CDCIL");
		btnDificultad2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnDificultad2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		btnDificultad2.setBounds(486, 300, 358, 163);
		panel_dificultad.add(btnDificultad2);
		
		JLabel lblSesionUsuario = new JLabel("Sesi\u00F3n iniciado como: ");
		lblSesionUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		lblSesionUsuario.setForeground(Color.WHITE);
		lblSesionUsuario.setFont(new Font("Dialog", Font.BOLD, 20));
		lblSesionUsuario.setBounds(10, 11, 488, 163);
		panel_dificultad.add(lblSesionUsuario);
		lblSesionUsuario.setText(lblSesionUsuario.getText() + NombreUsuario.toUpperCase());
		
		JButton btnVerEstadisticas = new JButton("Ver estad\u00EDsticas");
		btnVerEstadisticas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVerEstadisticas.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnVerEstadisticas.setFocusTraversalKeysEnabled(false);
		btnVerEstadisticas.setFocusPainted(false);
		btnVerEstadisticas.setBackground(Color.WHITE);
		btnVerEstadisticas.setBounds(623, 57, 294, 56);
		panel_dificultad.add(btnVerEstadisticas);
		
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaDificultad, "�Realmente quieres salir? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR EL PROGRAMA", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0)
					VentanaDificultad.dispatchEvent(new WindowEvent(VentanaDificultad, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaDificultad, "�Realmente quieres volver? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR SESI�N", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					inicio.getVentanaInicio().setVisible(true);
					VentanaDificultad.dispose();
				}
			}
		});
		
		btnVerEstadisticas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Dificultad.getVentanaDificultad().dispose();
				new Estadisticas(Dificultad.this, NombreUsuario);
			}
		});
		
		btnDificultad1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new Principal(Dificultad.this, 1, NombreUsuario);
			}
		});
		
		btnDificultad2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new Principal(Dificultad.this, 2, NombreUsuario);
			}
		});
		
	}
}
