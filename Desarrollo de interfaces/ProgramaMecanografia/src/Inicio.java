import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.Frame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;

import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.MatteBorder;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

public class Inicio {

	private JFrame VentanaInicio;
	private JTextField textUsuario;
	private JPasswordField passwordContrasena;
	private String usuarioNombre;

	public JFrame getVentanaInicio() {
		return VentanaInicio;
	}

	public void setVentanaInicio(JFrame VentanaInicio) {
		this.VentanaInicio = VentanaInicio;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio window = new Inicio();
					window.VentanaInicio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Inicio() {
		initialize();
	}

	private void initialize() {
		
		VentanaInicio = new JFrame();
		VentanaInicio.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\vacio.png"));
		VentanaInicio.getContentPane().setBackground(Color.BLACK);
		VentanaInicio.setTitle("Programa MECANOGRAF\u00CDA");
		VentanaInicio.setBounds(500,150,1000,700);
		VentanaInicio.setVisible(true);
		VentanaInicio.setResizable(false);
		VentanaInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		VentanaInicio.getContentPane().setLayout(null);
		
		JPanel panel_header = new JPanel();
		panel_header.setBackground(Color.BLACK);
		panel_header.setBounds(10, 11, 974, 139);
		VentanaInicio.getContentPane().add(panel_header);
		panel_header.setLayout(null);
		
		JLabel lblProgramaDeMecanografia = new JLabel("| PROGRAMA DE MECANOGRAF\u00CDA |");
		lblProgramaDeMecanografia.setForeground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setBackground(new Color(255, 255, 255));
		lblProgramaDeMecanografia.setHorizontalTextPosition(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProgramaDeMecanografia.setBorder(BorderFactory.createMatteBorder(2,15,2,15,Color.WHITE));
		lblProgramaDeMecanografia.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblProgramaDeMecanografia.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 35));
		lblProgramaDeMecanografia.setBounds(10, 11, 638, 80);
		panel_header.add(lblProgramaDeMecanografia);
		
		JLabel lblBarraHeader = new JLabel("");
		lblBarraHeader.setForeground(new Color(255, 255, 255));
		lblBarraHeader.setBackground(new Color(255, 255, 255));
		lblBarraHeader.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBarraHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblBarraHeader.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		lblBarraHeader.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		lblBarraHeader.setAlignmentX(0.5f);
		lblBarraHeader.setBounds(10, 102, 960, 29);
		panel_header.add(lblBarraHeader);
		
		JButton btnAcercaDe = new JButton("Acerca de");
		btnAcercaDe.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAcercaDe.setFocusPainted(false);
		btnAcercaDe.setFocusTraversalKeysEnabled(false);
		btnAcercaDe.setBackground(Color.WHITE);
		btnAcercaDe.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnAcercaDe.setBounds(803, 11, 161, 38);
		panel_header.add(btnAcercaDe);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSalir.setFocusPainted(false);
		btnSalir.setFocusTraversalKeysEnabled(false);
		btnSalir.setBackground(Color.WHITE);
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSalir.setBounds(803, 60, 161, 56);
		panel_header.add(btnSalir);
		
		JPanel panel_login = new JPanel();
		panel_login.setBackground(Color.BLACK);
		panel_login.setBounds(20, 161, 964, 499);
		VentanaInicio.getContentPane().add(panel_login);
		panel_login.setLayout(null);
		panel_login.setVisible(false);
		
		JLabel lblTituloLogin = new JLabel("= INICIO DE SESI\u00D3N =");
		lblTituloLogin.setHorizontalTextPosition(SwingConstants.CENTER);
		lblTituloLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloLogin.setForeground(Color.WHITE);
		lblTituloLogin.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 35));
		lblTituloLogin.setBackground(Color.WHITE);
		lblTituloLogin.setAlignmentX(0.5f);
		lblTituloLogin.setBounds(10, 11, 431, 80);
		panel_login.add(lblTituloLogin);
		
		JLabel lblBarraLogin = new JLabel("");
		lblBarraLogin.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBarraLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblBarraLogin.setForeground(Color.WHITE);
		lblBarraLogin.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		lblBarraLogin.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE));
		lblBarraLogin.setBackground(Color.WHITE);
		lblBarraLogin.setAlignmentX(0.5f);
		lblBarraLogin.setBounds(30, 62, 450, 29);
		panel_login.add(lblBarraLogin);
		
		JPanel login = new JPanel();
		login.setBorder(new MatteBorder(1, 10, 1, 1, (Color) new Color(0, 0, 0)));
		login.setBounds(20, 102, 805, 348);
		panel_login.add(login);
		login.setLayout(null);
		
		JLabel lblUsuario = new JLabel("USUARIO : ");
		lblUsuario.setBounds(212, 58, 259, 26);
		lblUsuario.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 25));
		lblUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		login.add(lblUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setToolTipText("Ingresa un usuario");
		textUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		textUsuario.setBorder(new MatteBorder(1, 5, 1, 1, (Color) new Color(0, 0, 0)));
		textUsuario.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		textUsuario.setColumns(10);
		textUsuario.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		textUsuario.setBounds(212, 95, 381, 51);
		login.add(textUsuario);
		
		JButton btnLogin = new JButton("Acceder");
		btnLogin.setForeground(new Color(255, 255, 255));
		btnLogin.setBackground(new Color(0, 0, 0));
		btnLogin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnLogin.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 25));
		btnLogin.setBounds(338, 270, 133, 51);
		login.add(btnLogin);
		
		JLabel labelPassword = new JLabel("CONTRASE\u00D1A : ");
		labelPassword.setHorizontalAlignment(SwingConstants.LEFT);
		labelPassword.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 25));
		labelPassword.setBounds(212, 157, 259, 26);
		login.add(labelPassword);
		
		passwordContrasena = new JPasswordField();
		passwordContrasena.setToolTipText("Ingresa la contrase\u00F1a");
		passwordContrasena.setEnabled(false);
		passwordContrasena.setHorizontalAlignment(SwingConstants.CENTER);
		passwordContrasena.setBorder(new MatteBorder(1, 5, 1, 1, (Color) new Color(0, 0, 0)));
		passwordContrasena.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		passwordContrasena.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		passwordContrasena.setBounds(212, 194, 381, 51);
		login.add(passwordContrasena);
		
		JPanel panel_logo = new JPanel();
		panel_logo.setBackground(Color.BLACK);
		panel_logo.setBounds(20, 161, 964, 499);
		VentanaInicio.getContentPane().add(panel_logo);
		panel_logo.setLayout(null);
		
		JLabel lblImagenLogo = new JLabel("");
		lblImagenLogo.setBounds(10, 11, 944, 342);
		lblImagenLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblImagenLogo.setIcon(new ImageIcon("img\\logo-inicio.png"));
		panel_logo.add(lblImagenLogo);	
		
		JPanel panel_barraCarga = new JPanel();
		panel_barraCarga.setBounds(154, 417, 598, 56);
		panel_logo.add(panel_barraCarga);
		panel_barraCarga.setLayout(null);
		
		JProgressBar progressBar = new JProgressBar(0, 200);
		progressBar.setBounds(0, 0, 598, 58);
		panel_barraCarga.add(progressBar);
		progressBar.setBackground(Color.WHITE);
		progressBar.setForeground(new Color(0, 204, 0));
		progressBar.setIndeterminate(true);
		progressBar.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 35));
		
		JLabel lblCargando = new JLabel("Estamos cargando el programa, espere ...");
		lblCargando.setBounds(154, 364, 598, 58);
		panel_logo.add(lblCargando);
		lblCargando.setForeground(Color.WHITE);
		lblCargando.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 25));
		lblCargando.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCargando.setHorizontalAlignment(SwingConstants.CENTER);
		
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(VentanaInicio, "�Realmente quieres salir? \nNOTA: PERDER�S TODO EL PROGRESO", "CERRAR EL PROGRAMA", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0)
					VentanaInicio.dispatchEvent(new WindowEvent(VentanaInicio, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		btnAcercaDe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JOptionPane.showMessageDialog(null, " Fecha: 08/10/2019\n Versi�n: 1.0\n Creador: Kacper Stanislaw Duda Duda \n\n Copyright \u00A9 | Todos los derechos reservados", "Acerca de", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		File txtLogin = new File("files\\login.txt");
		File txtDificultad = new File("files\\dificultad\\leccion.txt");
		
		if (txtLogin.exists() && txtDificultad.exists()) {
			
			TimerTask timer = new TimerTask() {
				public void run() {
					panel_logo.setVisible(false);
					panel_login.setVisible(true);	
				}
			};
			
			Timer temporizador = new Timer();
			temporizador.scheduleAtFixedRate(timer, 6000, 1000);
	
			textUsuario.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					passwordContrasena.setEnabled(true);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						btnLogin.doClick();
					}
				}
			});
			
			passwordContrasena.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						btnLogin.doClick();
					}
				}
			});
			
			
			btnLogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					File inputFichero = new File("files\\login.txt");
	
					boolean leido = false;
					String inputUsuario = textUsuario.getText();
					String inputContrasena = passwordContrasena.getText();
			 
					if ((textUsuario.getText().isEmpty()) || (passwordContrasena.getText().isEmpty()) || (textUsuario.getText().isEmpty() && passwordContrasena.getText().isEmpty())) {
						JOptionPane.showMessageDialog(null,"Login incorrecto, hay campos vac�os.", "Inicio de Sesi�n: ERROR", 0);
					} else {
						try {
							Scanner inF = new Scanner(inputFichero);
							while (inF.hasNext()) {
								String s = inF.nextLine();
								String[] sArray = s.split(":");
								
								usuarioNombre = sArray[0];
	
								if (sArray[0].equals(inputUsuario) && sArray[1].equals(inputContrasena)) {
									JOptionPane.showMessageDialog(null, "Has iniciado sesi�n correctamente.", "Inicio de sesi�n", JOptionPane.INFORMATION_MESSAGE);
									new Dificultad(Inicio.this, usuarioNombre);
									VentanaInicio.setVisible(false);
									leido = true;
									break;
								} else if (sArray[0] != inputUsuario && sArray[1] != inputContrasena) {
									leido = false;
								}
							}
							if (leido == false) {
								JOptionPane.showMessageDialog(null,	"Usuario o Contrase�a incorrectos", "Inicio de sesi�n: ERROR", JOptionPane.ERROR_MESSAGE);
							}
							inF.close();
						} catch (FileNotFoundException err) {
							System.out.println("Fichero de login y de dificultad no encontrado.");
						}  			
					}
					textUsuario.setText(null);
					passwordContrasena.setText(null);
				}
			});
		} else {
			// "files\login.txt" && "files\dificultad\leccion.txt"
			JOptionPane.showMessageDialog(null,	"Ficheros de configuracion no encontrados.", "Inicio de sesi�n: ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
}
