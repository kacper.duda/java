import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.JPanel;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JButton;
import java.awt.Insets;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;

public class GUI {

	private JFrame frmCalculadora;
	private char op =' ';
	int posicion;
	String num1;
	String num2;
	double total;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setTitle("CALCULADORA");
		frmCalculadora.setResizable(false);
		frmCalculadora.setBounds(100, 100, 431, 448);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTextField resultado = new JTextField("");
		resultado.setMargin(new Insets(30, 30, 30, 30));
		resultado.setEditable(false);
		resultado.setForeground(Color.BLACK);
		resultado.setFont(new Font("Tekton Pro", Font.PLAIN, 35));
		resultado.setBackground(Color.WHITE);
		resultado.setHorizontalAlignment(SwingConstants.CENTER);
		frmCalculadora.getContentPane().add(resultado, BorderLayout.NORTH);
		
		JPanel interfaz = new JPanel();
		frmCalculadora.getContentPane().add(interfaz, BorderLayout.CENTER);
		interfaz.setLayout(new GridLayout(0, 4, 0, 0));
		
		JButton btnNumero7 = new JButton("7");
		btnNumero7.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero7.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero7);
		
		JButton btnNumero8 = new JButton("8");
		btnNumero8.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero8.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero8);
		
		JButton btnNumero9 = new JButton("9");
		btnNumero9.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero9.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero9);
		
		JButton btnSumar = new JButton("+");
		btnSumar.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnSumar.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnSumar);
		
		JButton btnNumero4 = new JButton("4");
		btnNumero4.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero4.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero4);
		
		JButton btnNumero5 = new JButton("5");
		btnNumero5.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero5.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero5);
		
		JButton btnNumero6 = new JButton("6");
		btnNumero6.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero6.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero6);
		
		JButton btnRestar = new JButton("-");
		btnRestar.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnRestar.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnRestar);
		
		JButton btnNumero1 = new JButton("1");
		btnNumero1.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero1.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero1);
		
		JButton btnNumero2 = new JButton("2");
		btnNumero2.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero2.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero2);
		
		JButton btnNumero3 = new JButton("3");
		btnNumero3.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero3.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero3);
		
		JButton btnMultiplicar = new JButton("x");
		btnMultiplicar.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnMultiplicar.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnMultiplicar);
		
		JButton btnPunto = new JButton(".");
		btnPunto.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnPunto.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnPunto);
		
		JButton btnNumero0 = new JButton("0");
		btnNumero0.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnNumero0.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnNumero0);
		
		JButton btnCalcular = new JButton("=");
		btnCalcular.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnCalcular.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnCalcular);
		
		JButton btnDividir = new JButton("/");
		btnDividir.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		btnDividir.setMargin(new Insets(30, 30, 30, 30));
		interfaz.add(btnDividir);
		
		JPanel panel = new JPanel();
		frmCalculadora.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.setMargin(new Insets(15, 15, 15, 15));
		panel.add(btnBorrar);
		btnBorrar.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 20));
		
		// EVENTOS
		btnNumero0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "0");
			}
		});
		btnNumero1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "1");
			}
		});
		btnNumero2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "2");
			}
		});
		btnNumero3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "3");
			}
		});
		btnNumero4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "4");
			}
		});
		btnNumero5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "5");
			}
		});
		btnNumero6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "6");
			}
		});
		btnNumero7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "7");
			}
		});
		btnNumero8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "8");
			}
		});
		btnNumero9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "9");
			}
		});
		btnPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + ".");
				btnPunto.setEnabled(false);
			}
		});
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText("");
				btnRestar.setEnabled(true);
				btnMultiplicar.setEnabled(true);
				btnSumar.setEnabled(true);
				btnDividir.setEnabled(true);
				btnPunto.setEnabled(true);
			}
		});
		btnSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '+';
				resultado.setText(resultado.getText() + "+");
				btnRestar.setEnabled(false);
				btnMultiplicar.setEnabled(false);
				btnSumar.setEnabled(false);
				btnDividir.setEnabled(false);
				btnPunto.setEnabled(true);
			}
		});
		btnRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '-';
				resultado.setText(resultado.getText() + "-");
				btnRestar.setEnabled(false);
				btnMultiplicar.setEnabled(false);
				btnSumar.setEnabled(false);
				btnDividir.setEnabled(false);
				btnPunto.setEnabled(true);
			}
		});
		btnMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = 'x';
				resultado.setText(resultado.getText() + "x");
				btnRestar.setEnabled(false);
				btnMultiplicar.setEnabled(false);
				btnSumar.setEnabled(false);
				btnDividir.setEnabled(false);
				btnPunto.setEnabled(true);
			}
		});
		btnDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '/';
				resultado.setText(resultado.getText() + "/");
				btnRestar.setEnabled(false);
				btnMultiplicar.setEnabled(false);
				btnSumar.setEnabled(false);
				btnDividir.setEnabled(false);
				btnPunto.setEnabled(true);
			}
		});
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (op) {
				case '+': 
					posicion = resultado.getText().indexOf('+');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) + Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '-': 
					posicion = resultado.getText().indexOf('-');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) - Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case 'x': 
					posicion = resultado.getText().indexOf('x');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) * Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '/': 
					posicion = resultado.getText().indexOf('/');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) / Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				}
			}
		});
	}
}
