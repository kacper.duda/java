
public class Perro extends Animal {

	// raza , ladra, tipo

	// Atributos de la clase perro
	String raza;
	boolean ladra;
	String tipo;

	// Método constructor para crear objetos perro
	public Perro(String nombre, int edad, boolean chip, double peso, String color, String raza, boolean ladra,
			String tipo) {
		super(nombre, edad, chip, peso, color);
		this.raza = raza;
		this.ladra = ladra;
		this.tipo = tipo;
	}

	// Método Getters && Setters
	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public boolean isLadra() {
		return ladra;
	}

	public void setLadra(boolean ladra) {
		this.ladra = ladra;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	//Método ToString
	@Override
	public String toString() {
		return "Perro [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", raza=" + raza + ", ladra=" + ladra + ", tipo=" + tipo + "]";
	}
	
	

}
