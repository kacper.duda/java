
public class Gallina extends Animal {
	
	//NumHuevos, tieneCresta
	
	//Atributos de la clase gallina
	int numHuevos;
	boolean tieneCresta;
	
	// Método constructor para crear objetos Gallina
	public Gallina(String nombre, int edad, boolean chip, double peso, String color, int numHuevos,
			boolean tieneCresta) {
		super(nombre, edad, chip, peso, color);
		this.numHuevos = numHuevos;
		this.tieneCresta = tieneCresta;
	}

	// Método Getters && Setters
	public int getNumHuevos() {
		return numHuevos;
	}

	public void setNumHuevos(int numHuevos) {
		this.numHuevos = numHuevos;
	}

	public boolean isTieneCresta() {
		return tieneCresta;
	}

	public void setTieneCresta(boolean tieneCresta) {
		this.tieneCresta = tieneCresta;
	}
	
	//Método ToString
	@Override
	public String toString() {
		return "Gallina [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", numHuevos=" + numHuevos + ", tieneCresta=" + tieneCresta + "]";
	}
	
	


}
