
public class Conejo extends Animal{
	
	//tamaño, pelo, campero
	
	//Atributos para la clase conejo
	double tama�o;
	char pelo; // 'R' rizado, 'L' liso, 
	boolean campero;
	
	// Método constructor para crear objetos Conejo
	public Conejo(String nombre, int edad, boolean chip, double peso, String color, double tama�o, char pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tama�o = tama�o;
		this.pelo = pelo;
		this.campero = campero;
	}
	// Método Getters && Setters
	public double getTama�o() {
		return tama�o;
	}

	public void setTama�o(double tama�o) {
		this.tama�o = tama�o;
	}

	public char getPelo() {
		return pelo;
	}

	public void setPelo(char pelo) {
		this.pelo = pelo;
	}

	public boolean isCampero() {
		return campero;
	}

	public void setCampero(boolean campero) {
		this.campero = campero;
	}
	
	//Método ToString
	@Override
	public String toString() {
		return "Conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tama�o=" + tama�o + ", pelo=" + pelo + ", campero=" + campero + "]";
	}


	

}
