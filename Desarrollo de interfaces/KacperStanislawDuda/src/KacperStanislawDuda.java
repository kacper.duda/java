import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.ComponentOrientation;
import java.awt.Insets;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ItemListener;
import java.lang.Thread.State;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DropMode;
import javax.swing.JTextArea;

public class KacperStanislawDuda {

	private JFrame Ventana;
	private JTextField fieldUsuario;
	private JTextField fieldPassword;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KacperStanislawDuda window = new KacperStanislawDuda();
					window.Ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public KacperStanislawDuda() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Ventana = new JFrame();
		Ventana.setTitle("Registro de Usuario");
		Ventana.setBounds(100, 100, 556, 415);
		Ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Ventana.getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUsuario.setBounds(46, 31, 73, 31);
		Ventana.getContentPane().add(lblUsuario);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(46, 73, 73, 31);
		Ventana.getContentPane().add(lblPassword);
		
		fieldUsuario = new JTextField();
		fieldUsuario.setBounds(129, 31, 173, 27);
		Ventana.getContentPane().add(fieldUsuario);
		fieldUsuario.setColumns(10);
		
		fieldPassword = new JTextField();
		fieldPassword.setColumns(10);
		fieldPassword.setBounds(129, 80, 173, 27);
		Ventana.getContentPane().add(fieldPassword);
		
		JRadioButton rdbtnA�os1 = new JRadioButton("Menor de 18 a\u00F1os");
		rdbtnA�os1.setSelected(true);
		buttonGroup_1.add(rdbtnA�os1);
		rdbtnA�os1.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnA�os1.setBounds(139, 131, 173, 23);
		Ventana.getContentPane().add(rdbtnA�os1);
		
		JRadioButton rdbtnA�os2 = new JRadioButton("A partir de 18 a\u00F1os");
		buttonGroup_1.add(rdbtnA�os2);
		rdbtnA�os2.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnA�os2.setBounds(139, 165, 173, 23);
		Ventana.getContentPane().add(rdbtnA�os2);
		
		JRadioButton rdbtnTerminos1 = new JRadioButton("Ver t\u00E9rminos");
		rdbtnTerminos1.setSelected(true);
		buttonGroup.add(rdbtnTerminos1);
		rdbtnTerminos1.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnTerminos1.setBounds(314, 131, 173, 23);
		Ventana.getContentPane().add(rdbtnTerminos1);
		
		JRadioButton rdbtnTerminos2 = new JRadioButton("Ocultar t\u00E9rminos");
		buttonGroup.add(rdbtnTerminos2);
		rdbtnTerminos2.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnTerminos2.setBounds(314, 165, 173, 23);
		Ventana.getContentPane().add(rdbtnTerminos2);
		
		JCheckBox chckbxAceptoTerminos = new JCheckBox("Acepto los t\u00E9rminos");
		chckbxAceptoTerminos.setEnabled(false);
		chckbxAceptoTerminos.setFont(new Font("Tahoma", Font.BOLD, 12));
		chckbxAceptoTerminos.setBounds(139, 224, 163, 23);
		Ventana.getContentPane().add(chckbxAceptoTerminos);
		
		JButton btnRegistrar = new JButton("Registrar Usuario");
		btnRegistrar.setEnabled(false);
		btnRegistrar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRegistrar.setBounds(139, 318, 163, 23);
		Ventana.getContentPane().add(btnRegistrar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(314, 224, 185, 119);
		Ventana.getContentPane().add(scrollPane);
		
		JTextArea txtrTrminosYCondiciones = new JTextArea();
		txtrTrminosYCondiciones.setWrapStyleWord(true);
		scrollPane.setViewportView(txtrTrminosYCondiciones);
		txtrTrminosYCondiciones.setLineWrap(true);
		txtrTrminosYCondiciones.setText("T\u00E9rminos y Condiciones del Contrato: Las presentes Condiciones Generales de contrataci\u00F3n, junto con las Condiciones Particulares que, en cada caso y servicio, pueda establecerse (en lo sucesivo, y en conjunto, las \"Condiciones de Contrataci\u00F3n\") regulan expresamente las relaciones surgidas entre ambas partes.");
		
		// EVENTOS
		
		// A
		rdbtnA�os2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chckbxAceptoTerminos.setEnabled(true);
			}
		});
		rdbtnA�os1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chckbxAceptoTerminos.setEnabled(false);
				chckbxAceptoTerminos.setSelected(false);
				btnRegistrar.setEnabled(false);
			}
		});
		
		// B
		rdbtnTerminos2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtrTrminosYCondiciones.setVisible(false);
				scrollPane.setVisible(false);
			}
		});
		rdbtnTerminos1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtrTrminosYCondiciones.setVisible(true);
				scrollPane.setVisible(true);
			}
		});
		
		// C
		chckbxAceptoTerminos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxAceptoTerminos.isSelected()) {
					btnRegistrar.setEnabled(true);
				} else {
					btnRegistrar.setEnabled(false);
				}
			}
		});
		
		// D
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ((fieldUsuario.getText().isEmpty()) || (fieldPassword.getText().isEmpty()) || (fieldUsuario.getText().isEmpty() && fieldPassword.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null,"Registro incorrecto, datos incompletos", "Registro", 0);
				} else {
					JOptionPane.showMessageDialog(null,"Registro correcto", "Registro", 1);
				}
			}
		});
	}
}
