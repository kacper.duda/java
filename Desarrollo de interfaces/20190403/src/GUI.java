import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Color;
import java.awt.Window.Type;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.border.TitledBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JSeparator;
import java.awt.Toolkit;
import com.toedter.calendar.JCalendar;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GUI {

	// Un JFrame representa a la ventana en su totalidad
	private JFrame frmInicioDeSesion;
	private JTextField txtUsuario;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmInicioDeSesion.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmInicioDeSesion = new JFrame();
		frmInicioDeSesion.setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\GIT\\java\\20190403\\img\\img-log.png"));
		frmInicioDeSesion.setResizable(false);
		frmInicioDeSesion.setType(Type.POPUP);
		frmInicioDeSesion.setFont(new Font("Adobe Fan Heiti Std B", Font.BOLD, 16));
		frmInicioDeSesion.setBackground(Color.RED);
		frmInicioDeSesion.setTitle("INICIO DE SESI\u00D3N");
		frmInicioDeSesion.getContentPane().setFont(new Font("Stencil", Font.PLAIN, 11));
		frmInicioDeSesion.setBounds(100, 100, 1071, 778);
		frmInicioDeSesion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmInicioDeSesion.getContentPane().setLayout(null);
		frmInicioDeSesion.setLocationRelativeTo(null);
		
		JButton aceptar = new JButton("Aceptar");
		aceptar.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
		aceptar.setForeground(new Color(255, 255, 255));
		aceptar.setBackground(Color.BLACK);
		aceptar.setToolTipText("");
		aceptar.setBounds(798, 670, 207, 43);
		frmInicioDeSesion.getContentPane().add(aceptar);
		
		txtUsuario = new JTextField();
		txtUsuario.setToolTipText("Usuario");
		txtUsuario.setBounds(45, 307, 224, 20);
		frmInicioDeSesion.getContentPane().add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Introduzca usuario:");
		lblNewLabel.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setBounds(45, 282, 224, 14);
		frmInicioDeSesion.getContentPane().add(lblNewLabel);
		
		JLabel imagen = new JLabel("");
		imagen.setToolTipText("Patata");
		imagen.setIcon(new ImageIcon("E:\\GIT\\java\\20190403\\img\\gui-icon.png"));
		imagen.setBounds(45, 46, 224, 225);
		frmInicioDeSesion.getContentPane().add(imagen);
		
		JCheckBox acepto = new JCheckBox("Acepto las condiciones");
		acepto.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		acepto.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		acepto.setBounds(798, 637, 207, 23);
		frmInicioDeSesion.getContentPane().add(acepto);
		
		JRadioButton alumno = new JRadioButton("Alumno");
		alumno.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		buttonGroup_1.add(alumno);
		alumno.setBounds(287, 346, 91, 23);
		frmInicioDeSesion.getContentPane().add(alumno);
		
		JRadioButton profesor = new JRadioButton("Profesor");
		profesor.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		buttonGroup_1.add(profesor);
		profesor.setBounds(392, 346, 91, 23);
		frmInicioDeSesion.getContentPane().add(profesor);
		
		JRadioButton hombre = new JRadioButton("Hombre");
		hombre.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		buttonGroup.add(hombre);
		hombre.setBounds(287, 306, 82, 23);
		frmInicioDeSesion.getContentPane().add(hombre);
		
		JRadioButton mujer = new JRadioButton("Mujer");
		mujer.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		buttonGroup.add(mujer);
		mujer.setBounds(392, 306, 82, 23);
		frmInicioDeSesion.getContentPane().add(mujer);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MADRID", "BARCELONA", "VALENCIA", "SEVILLA", "BILBAO", "ZARAGOZA", "ALBACETE"}));
		comboBox.setBounds(45, 414, 224, 20);
		frmInicioDeSesion.getContentPane().add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 470, 224, 174);
		frmInicioDeSesion.getContentPane().add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setLineWrap(true);
		
		JLabel lblSeleccioneCiudad = new JLabel("Seleccione ciudad:");
		lblSeleccioneCiudad.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		lblSeleccioneCiudad.setHorizontalAlignment(SwingConstants.LEFT);
		lblSeleccioneCiudad.setBounds(44, 391, 224, 14);
		frmInicioDeSesion.getContentPane().add(lblSeleccioneCiudad);
		
		JLabel lblObservaciones = new JLabel("Observaciones:");
		lblObservaciones.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		lblObservaciones.setHorizontalAlignment(SwingConstants.LEFT);
		lblObservaciones.setBounds(45, 445, 129, 14);
		frmInicioDeSesion.getContentPane().add(lblObservaciones);
		
		passwordField = new JPasswordField();
		passwordField.setEnabled(false);
		passwordField.setToolTipText("Contrase\u00F1a");
		passwordField.setEchoChar('*');
		passwordField.setBounds(44, 358, 224, 22);
		frmInicioDeSesion.getContentPane().add(passwordField);
		
		JLabel lblIntroduzcaContrasea = new JLabel("Introduzca contrase\u00F1a:");
		lblIntroduzcaContrasea.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		lblIntroduzcaContrasea.setHorizontalAlignment(SwingConstants.LEFT);
		lblIntroduzcaContrasea.setBounds(44, 333, 225, 14);
		frmInicioDeSesion.getContentPane().add(lblIntroduzcaContrasea);
		
		JLabel lblAficiones = new JLabel("Aficiones:");
		lblAficiones.setHorizontalAlignment(SwingConstants.LEFT);
		lblAficiones.setFont(new Font("Charlemagne Std", Font.PLAIN, 11));
		lblAficiones.setBounds(287, 445, 129, 14);
		frmInicioDeSesion.getContentPane().add(lblAficiones);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(287, 470, 160, 174);
		frmInicioDeSesion.getContentPane().add(scrollPane_1);
		
		JList list = new JList();
		scrollPane_1.setViewportView(list);
		list.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		list.setFont(new Font("Tahoma", Font.PLAIN, 14));
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"VIDEOJUEGOS", "M\u00DASICA", "CINE", "DEPORTES", "VIAJES", "COCINAR", "SALIR DE FIESTA", "IR DE COMPRAS"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		JSeparator separator = new JSeparator();
		separator.setBounds(287, 336, 207, 2);
		frmInicioDeSesion.getContentPane().add(separator);
		
		JCalendar calendar = new JCalendar();
		calendar.setBounds(478, 480, 184, 153);
		frmInicioDeSesion.getContentPane().add(calendar);
		
		
		// EVENTOS
		
		txtUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				passwordField.setEnabled(true);
			}
		});
		
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				acepto.setSelected(true);
			}
		});
		
		frmInicioDeSesion.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				profesor.setSelected(true);
				mujer.setSelected(true);
			}
		});
		
		alumno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(frmInicioDeSesion,
						"¿Realmente eres un alumno?", "Confirmación",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
						if (respuesta == 0)
							alumno.setSelected(true);
						else
							alumno.setSelected(false);
			}
		});
		
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (comboBox.getSelectedIndex() == 1)
				imagen.setIcon(new ImageIcon("E:\\GIT\\java\\20190403\\img\\img-log.png"));
			}
		});
		
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!txtUsuario.getText().isEmpty()) {
				}
			}
		});
	}
}
