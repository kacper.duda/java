import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Window.Type;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import javax.swing.JButton;

public class gui {

	private JFrame ventanaAplicacion;
	public JFrame getVentanaAplicacion() {
		return ventanaAplicacion;
	}

	public void setVentanaAplicacion(JFrame ventanaAplicacion) {
		this.ventanaAplicacion = ventanaAplicacion;
	}

	private JTextField textoUsuario;
	private JPasswordField textPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui window = new gui();
					window.ventanaAplicacion.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		ventanaAplicacion = new JFrame();
		ventanaAplicacion.setBounds(100, 100, 1078, 790);
		ventanaAplicacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaAplicacion.getContentPane().setLayout(null);
		
		JLabel tituloTienda = new JLabel("TIENDA");
		tituloTienda.setHorizontalAlignment(SwingConstants.CENTER);
		tituloTienda.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD | Font.ITALIC, 30));
		tituloTienda.setBounds(72, 42, 182, 86);
		ventanaAplicacion.getContentPane().add(tituloTienda);
		
		JLabel labelUsuario = new JLabel("| Usuario: ");
		labelUsuario.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 20));
		labelUsuario.setBounds(72, 144, 120, 23);
		ventanaAplicacion.getContentPane().add(labelUsuario);
		
		JLabel labelPassword = new JLabel("| Contrase\u00F1a: ");
		labelPassword.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 20));
		labelPassword.setBounds(72, 191, 138, 21);
		ventanaAplicacion.getContentPane().add(labelPassword);
		
		textoUsuario = new JTextField();
		textoUsuario.setToolTipText("Introduzca tu nombre de usuario");
		textoUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textoUsuario.setBounds(215, 139, 169, 35);
		ventanaAplicacion.getContentPane().add(textoUsuario);
		textoUsuario.setColumns(10);
		
		textPassword = new JPasswordField();
		textPassword.setToolTipText("Introduzca tu contrase\u00F1a");
		textPassword.setEnabled(false);
		textPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textPassword.setEchoChar('*');
		textPassword.setBounds(215, 185, 169, 35);
		ventanaAplicacion.getContentPane().add(textPassword);
		
		JCheckBox checkRecordarPassword = new JCheckBox("Recordar contrase\u00F1a");
		checkRecordarPassword.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		checkRecordarPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		checkRecordarPassword.setBounds(165, 227, 219, 23);
		ventanaAplicacion.getContentPane().add(checkRecordarPassword);
		
		JButton btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnIniciarSesion.setBounds(215, 271, 169, 35);
		ventanaAplicacion.getContentPane().add(btnIniciarSesion);
		
		JButton btnRegistro = new JButton("Registrarse");
		btnRegistro.setBounds(23, 271, 169, 35);
		ventanaAplicacion.getContentPane().add(btnRegistro);
		
		// EVENTOS
		
		textoUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				textPassword.setEnabled(true);
			}
		});
		
		checkRecordarPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(ventanaAplicacion,
						"�Est�s seguro que quieres recordar la contrase�a?", "| Recordar Contrase�a |",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
						if (respuesta == 0)
							checkRecordarPassword.setSelected(true);
						else
							checkRecordarPassword.setSelected(false);
			}
		});
	}
}
