
public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Coche coche1 = new Coche("1234CVH", "Volvo", "S60", 25000, 'D', 123, 5, false);
		Coche coche2 = new Coche("5678KNY", "Mercedes", "CLA", 50000, 'G', 344, 5, true);
		Coche coche3 = new Coche("6691GJR", "Kia", "Sportage", 21340, 'D', 520, 4, false);
		Coche coche4 = new Coche("3760FWX", "Opel", "Astra", 26000, 'H', 277, 5, false);
		Coche coche5 = new Coche("2298BBC", "Volvo", "FH16", 100000, 'D', 840, 6, true);
		Coche coche6 = new Coche("4001JSN", "Citroen", "Berlingo", 13000, 'G', 631, 5, false);
		Coche coche7 = new Coche("9303DFD", "Suzuki", "Ninja", 48000, 'G', 559, 2, true);
		Coche coche8 = new Coche("7762GHT", "Renault", "Clio", 16500, 'D', 215, 4, false);

		Coche[][] arraydeCoches = new Coche[2][4];

		arraydeCoches[1][1] = coche1;
		arraydeCoches[0][3] = coche2;
		arraydeCoches[0][0] = coche3;
		arraydeCoches[1][2] = coche4;
		arraydeCoches[0][1] = coche5;
		arraydeCoches[1][0] = coche6;
		arraydeCoches[0][2] = coche7;
		arraydeCoches[1][3] = coche8;

		//Primer for recorre filas
		for (int fila = 0; fila < arraydeCoches.length; fila++) {
			// Segundo for recorre columnas
			for (int col = 0; col < arraydeCoches[fila].length; col++) {
				System.out.print(arraydeCoches[fila][col].getMarca() + " " + arraydeCoches[fila][col].getModelo() + "\t");
			}
			System.out.print(" \n");

		}
		System.out.print(" \n");
		
		
		/*System.out.println("Precio" + " " + coche1.getPrecio() + "  " + "y con IVA" + " " + coche1.calcularIVA());
		System.out.println("Tu coche es " + " " + coche1.getTipo() + " " + "la categoria ambiental es" + " " + coche1.categoria());
		
		
		coche1.pinchaRueda();
		System.out.println("rueda pinchada" + " " +coche1.getRuedas());*/
	}
}
