
public class Camion extends Vehiculo {

	// Atributos de la clase camión
	double peso;
	int numeroEjes;
	boolean remolque;
	double capacidad;
	char se�al;

	// Método constructor para crear objetos moto
	public Camion(String matricula, String marca, String modelo, double precio, double peso, int numeroEjes,
			boolean remolque, double capacidad, char se�al) {
		super(matricula, marca, modelo, precio);
		this.peso = peso;
		this.numeroEjes = numeroEjes;
		this.remolque = remolque;
		this.capacidad = capacidad;
		this.se�al = se�al;
	}

	// M�todo Getters && Setters
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public int getNumeroEjes() {
		return numeroEjes;
	}

	public void setNumeroEjes(int numeroEjes) {
		this.numeroEjes = numeroEjes;
	}

	public boolean isRemolque() {
		return remolque;
	}

	public void setRemolque(boolean remolque) {
		this.remolque = remolque;
	}

	public double getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}

	public char getSe�al() {
		return se�al;
	}

	public void setSe�al(char se�al) {
		this.se�al = se�al;
	}

	// M�todo ToString Para camión
	@Override
	public String toString() {
		return "Camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", peso=" + peso + ", numeroEjes=" + numeroEjes + ", remolque=" + remolque + ", capacidad="
				+ capacidad + ", se�al=" + se�al + "]";
	}
}
