
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creación de un objeto coche

		Coche coche1 = new Coche("123DFW", "Audi", "A3", 21340.65, 'D', 123459876, 5, false);
		Coche coche2 = new Coche("563JDA", "Ford", "Focus", 18225.82, 'G', 123569874, 5, true);

		System.out.println(coche1);
		System.out.println(coche2.getPrecio());
		System.out.println(coche1.getTipo());
		System.out.println(coche1.getMarca());

		coche1.setPrecio(15000);
		System.out.println(coche1.getPrecio());

		int[] array = { 1, 3, 4, 7, 9, -2, 0, 7 };

		Coche[] arrayDeCoche = { coche1, coche2 };

		for (int i = 0; i < arrayDeCoche.length; i++) {
			System.out.println(arrayDeCoche[i]);

		}
		System.out.print("\n");
		
		
		// Matriz de numeros
		int[][] matriz = { { 1, 3, 0, -2, 7 }, { 4, 9, 9, 5, 6 }, { 8, 1, 3, 4, 5 } };

		// Primer for recorre filas
		for (int i = 0; i < matriz.length; i++) {
			// Segundo for recorre columnas
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + "\t");
			}
		System.out.print(" \n");
		}
	}

}
