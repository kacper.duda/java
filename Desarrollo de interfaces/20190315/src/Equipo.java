
public class Equipo {
	
	//Equipo -> Nombre, plantilla, Fecha de Creacion, Presupuesto, estadio

	String nombre;
	int plantilla;
	int a�os;
	double presupuesto;
	boolean estadio;
	char tipo; //F -> futbol, B -> Baloncesto

	
	//Metodo constructor
	
	public Equipo(String nombre, int jugadores, int a�os, double presupuesto, boolean estadio, char tipo) {
		super();
		this.nombre = nombre;
		this.plantilla = jugadores;
		this.a�os = a�os;
		this.presupuesto = presupuesto;
		this.estadio = estadio;
		this.tipo = tipo;
	}

	//M�todos getters && setters
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getPlantilla() {
		return plantilla;
	}


	public void setPlantilla(int plantilla) {
		this.plantilla = plantilla;
	}


	public int getFechaCreacion() {
		return a�os;
	}


	public void setFechaCreacion(int a�os) {
		this.a�os = a�os;
	}


	public double getPresupuesto() {
		return presupuesto;
	}


	public void setPresupuesto(double presupuesto) {
		this.presupuesto = presupuesto;
	}


	public boolean isEstadio() {
		return estadio;
	}


	public void setEstadio(boolean estadio) {
		this.estadio = estadio;
	}
	
	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	//M�todo TOSTRINGss
	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", plantilla=" + plantilla + ", a�os=" + a�os + ", presupuesto="
				+ presupuesto + ", estadio=" + estadio + ", tipo=" + tipo + "]";
	}
	



	

	
	
	

	
}
