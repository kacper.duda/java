import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * MENU: 
 * 		1.	Aparcar coche 	(5�)
 * 		2.	Aparcar moto 	(2�)
 * 		3.	Aparcar camion	(8�)
 * 		4.	Ver recaudaci�n
 * 		5.	Salir
 * 
 * 	Prueba de ejecuci�n: 1,1,3,2,1,2,3 -> 35�
 */

public class Principal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// MENU
		int recaudacion = 0;
		int n = 0;
		do {
			System.out.println("\n");
			System.out.println("------------------------------------");
			System.out.println("|	Men� Calcular Area	   |");
			System.out.println("------------------------------------");
			System.out.println("1. Aparcar coche	(5�)");
			System.out.println("2. Aparcar moto		(2�)");
			System.out.println("3. Aparcar cami�n	(8�)");
			System.out.println("4. Ver recaudaci�n");
			System.out.println("5. Salir");
			System.out.println("\n");
			System.out.println("Elija una opci�n: ");

			// Pide datos
			@SuppressWarnings("resource") // QUITA LOS WARNING DE UN RECURSO QUE ESTE DEBAJO
			Scanner teclado = new Scanner(System.in);
			try {
				n = teclado.nextInt();
			} catch (InputMismatchException error) {
				System.out.println("Introduzca la opcion de forma correcta.");
			}

			System.out.println("\n");

			switch (n) {
			case 1:
				System.out.println("Ha solicitado aparcar coche: \n");
				System.out.println("\t Inserte la matricula de su coche: ");
				String matriculacoche = teclado.next();
				System.out.println("La matr�cula es " + matriculacoche);
				Coche coche1 = new Coche(matriculacoche, "Ford", "Focus", 18225.82, 'G', 123569874, 5, true);
				recaudacion += 5;
				break;
			case 2:
				System.out.println("Ha solicitado aparcar moto: \n");
				System.out.println("\t Inserte la matricula de su moto: ");
				String matriculamoto = teclado.next();
				System.out.println("La matr�cula es " + matriculamoto);
				Moto moto1 = new Moto(matriculamoto,"Yamaha", "G35", 18000, 500, 4, false);
				recaudacion += 2;
				break;
			case 3:
				System.out.println("Ha solicitado aparcar camion: \n");
				System.out.println("\t Inserte la matricula de su camion: ");
				String matriculacamion = teclado.next();
				System.out.println("La matr�cula es " + matriculacamion);
				Camion camion1 = new Camion(matriculacamion, "Daf", "730T", 85000, 5.5, 6, true, 2000, 'C');
				recaudacion += 8;
				break;
			case 4:
				System.out.println("Ver recaudaci�n: \n");
				System.out.println("La recaudacion total actual es: " + recaudacion);
				break;
			case 5:
				System.out.println("Hasta luego. \n");
				break;
			default:
				System.out.println("Error, elija una opci�n v�lida.\n ");
				break;

			}

		} while (n != 5);

	}

}
